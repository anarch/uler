<?php
/* Uler settings 22/11/03 */
// Timezone
date_default_timezone_set('Asia/Tokyo');

// upload info limits
$upfilenamelimit = 244;
$upauthorlimit = 32;
$uptitlelimit = 64;
$upcommentlimit = 2048;

/*
upload filesize limit will decided in php.ini
please check:
1. upload_max_filesize is not too small, it will decide size limit
2. post_max_size is bigger than upload_max_filesize
3. memory_limit is bigger than post_max_size
4. upload_tmp_dir is not full

also you need to check SecRequestBodyLimit(Apache)

after changing those settings, you need to restart php fpm.
systemctl restart php7.4-fpm

php execution time limit is also important.
if your server has 10MB/s compressing speed and default time limit(30s)
zip file limit will be 10*30=300MB
*/

// script path
// http://www.example.com/uler/
$scpath = "http://www.example.com/uler/";

// db path
$dbfilename = "images/uler.db";

// zip temporary folder path
$zippath = "images/";

// file ext. allow list
$allowext = ["jpg", "jpeg", "png", "gif", "bmp", "wbmp", "xbm", "zip", "lzh", "txt", "doc", "docx", "pptx", "psd", "wav", "mp3", "flac", "mid", "mp4", "pdf", "heic"];

// header
$header = <<<EOD
<html><head><title>%title%</title><meta http-equiv="Cache-Control" content="no-cache"></head><body>
Uler 22/11/03 <a href="../list/">gallery</a> <a href="../ul/">upload</a> <a href="../user/tag">tag</a> <a href="../view/zip">zip</a><br>
<hr>
EOD;

// footer
$footer = <<<EOD
<hr>
<a href="%back%">back</a>
</body>
</html>
EOD;

// default title
$headertitle = "Uler";

// default back dest
$backto = "../";

// upload form
$upform = <<<EOD
<form action="./" method="POST" enctype="multipart/form-data"> Max filesize: 16M or 60sec<br>
<input type="file" name="userfile"><br>
Title:
<input type="text" name="title"><br>
Author(Required):
<input type="text" name="author"><br>
Tag:
<input type="text" name="tag"><br>
Comment:
<textarea name="comment"></textarea><br>
View password:
<input type="text" name="viewpass"><br>
Delete password(leave blank to free delete):
<input type="text" name="delpass"><br>
<input type="submit">
EOD;

