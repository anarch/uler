import React from "react";
import { Box, Button, Container, TextField, Paper, Table, TableBody, 
        TableCell, TableContainer, TableHead, TableRow, Divider, Pagination, 
        Switch, Grid, List, ListItem, ListItemText, Chip, Dialog, DialogTitle,
        DialogContent, DialogContentText, DialogActions, Alert, AlertTitle, Snackbar,
        AppBar, Toolbar, Typography, Select, MenuItem, FormControl, InputLabel,
        FormHelperText, FormGroup, FormControlLabel, ThemeProvider, createTheme, CssBaseline, Fade, Collapse, CircularProgress } from "@mui/material";
import axios from "axios";

class UploadForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedfile: '',
            fileselected: false,
            title: '',
            author: '',
            tag: '',
            comment: '',
            viewpass: '',
            delpass: '',
            openerror: false,
            errormessage: 'エラーが発生しました。',
            opennotice: false,
            noticemessage: '成功しました。',
            uploading: false,
            uploadwarn: false,
        }
    }

    checkWarn(name, tag) {
        const util = new TableTime;
        var regex = new RegExp("^(提出)");
        if (tag.match(regex)) {
            const ext = util.getExt(name);
            if (ext == "jpg" || ext == "jpeg" || ext == "mp3") {
                this.setState({uploadwarn: true});
            } else {
                this.setState({uploadwarn: false});
            }
        } else {
            this.setState({uploadwarn: false});
        }
    }

    onChangeFileSelect(e) {
        this.setState({selectedfile: e.target.files.item(0)});
        this.checkWarn(e.target.files.item(0).name, this.state.tag);
        this.setState({fileselected: true});
    }

    onChangeTitle(e) {
        this.setState({title: e.target.value});
    }

    onChangeAuthor(e) {
        this.setState({author: e.target.value});
    }

    onChangeTag(e) {
        this.setState({tag: e.target.value});
        this.checkWarn(this.state.selectedfile.name, e.target.value);
    }

    onChangeComment(e) {
        this.setState({comment: e.target.value});
    }

    onChangeViewpass(e) {
        this.setState({viewpass: e.target.value});
    }

    onChangeDelpass(e) {
        this.setState({delpass: e.target.value});
    }

    checkUploadForm() {
        if (this.state.author == '') {
            this.setState({errormessage: '作者名を入力してください。'});
            return false;
        } else {
            return true;
        }
    }

    handleUpload() {
        var self = this;
        console.log(this.state.fileselected);
        if (this.state.fileselected) {
            if (!this.checkUploadForm()) {
                this.handleErrorSnackOpen();
                return false;
            }
            this.setState({uploading: true});
            const params = new FormData();
            params.append('json', true);
            params.append('userfile', this.state.selectedfile);
            params.append('title', this.state.title);
            params.append('author', this.state.author);
            params.append('tag', this.state.tag);
            params.append('comment', this.state.comment);
            params.append('viewpass', this.state.viewpass);
            params.append('delpass', this.state.delpass);
            const headers = {headers: {'Content-Type': 'multipart/form-data',}};
            // get list
            axios.post(this.props.path + 'ul/', params, headers)
                .then(function (response) {
                    self.setState({uploading: false});
                    const resp = response.data;
                    console.log(resp);
                    if (resp > -1) {
                        self.setState({noticemessage: '成功しました。'});
                        self.handleNoticeSnackOpen();
                    } else {
                        self.setState({errormessage: '失敗しました。'});
                        self.handleErrorSnackOpen();
                    }
                }).catch(function (error) {
                    self.setState({uploading: false});
                    console.log(error);
                    const code = error.code;
                    if (code == "ERR_NETWORK") {
                        self.setState({errormessage: 'ネットワークエラーです。'});
                    } else {
                        const resp = error.response.status;
                        if (resp == 413) {
                            self.setState({errormessage: 'ファイルサイズが大きすぎます。'});
                        }
                    }
                    self.handleErrorSnackOpen();
                });
        } else {
            this.setState({errormessage: 'ファイルを選択してください。'});
            this.handleErrorSnackOpen();
        }
    }

    formFile() {
        return(
            <Button component="label" variant="outlined">選択…<input type="file" style={{display: "none"}} onChange={(e) => this.onChangeFileSelect(e)} /></Button>
        );
    }

    onEnterKey(key) {
        if (!this.state.uploading) {
            if (key == 'Enter' || key == null) {
                this.handleUpload();
            }
        }
    }

    formSubmit() {
        if (this.state.uploading) {
            return(
                <Button variant="contained" sx={{ width: 200 }} disabled>送信中…</Button>
            );
        } else if(this.state.opennotice) {
            return(
                <Button variant="contained" color="success" sx={{ width: 200 }} onClick={() => this.handleNoticeSnackClose()}>成功</Button>
            );
        } else if(this.state.openerror) {
            return(
                <Button variant="contained" color="warning" sx={{ width: 200 }} onClick={() => this.handleNoticeSnackClose()}>失敗</Button>
            );
        } else {
            return(
                <Button variant="contained" sx={{ width: 200 }} onClick={() => this.handleUpload()}>送信</Button>
            );
        }
    }

    handleErrorSnackOpen() {
        this.setState({openerror: true});
    }

    handleErrorSnackClose() {
        this.setState({openerror: false});
    }

    handleNoticeSnackOpen() {
        this.setState({opennotice: true});
    }

    handleNoticeSnackClose() {
        this.setState({opennotice: false});
    }

    renderSnackbars() {
        return(
            <Box>
            <Snackbar open={this.state.openerror} onClose={() => this.handleErrorSnackClose()}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
                <Alert onClose={() => this.handleErrorSnackClose()} severity="error">
                <AlertTitle>エラー</AlertTitle>
                {this.state.errormessage}
            </Alert>
            </Snackbar>
            <Snackbar open={this.state.opennotice} onClose={() => this.handleNoticeSnackClose()}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
                <Alert onClose={() => this.handleNoticeSnackClose()} severity="success">
                <AlertTitle>成功</AlertTitle>
                {this.state.noticemessage}
            </Alert>
            </Snackbar>
            </Box>
        );
    }

    displayProgress() {
        if (this.state.uploading) {
            return(<CircularProgress />);
        }
    }

    formInputs() {
        return(
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    ファイルを選択してください：{this.formFile()}
                    {this.state.selectedfile.name}
                    {this.state.uploadwarn ? <Typography variant="subtitle2">
                        <br />元データより劣化する可能性のある形式です。PNGもしくはWAVファイルでの提出を推奨します。
                    </Typography> : false}
                </Grid>
                <Grid item xs={6}>
                    <TextField id="ultitle" label="タイトル" fullWidth helperText="64文字まで" onChange={(e) => this.onChangeTitle(e)} />
                </Grid>
                <Grid item xs={6}>
                    <TextField id="ulauthor" required label="作者名" fullWidth helperText="32文字まで" onChange={(e) => this.onChangeAuthor(e)} onKeyDown={(e) => this.onEnterKey(e.key)} />
                </Grid>
                <Grid item xs={8}>
                    <TextField id="ulcomment" label="コメント" helperText="2048文字まで" multiline fullWidth rows={4} onChange={(e) => this.onChangeComment(e)} />
                </Grid>
                <Grid item xs={4}>
                    <TextField id="ultag" label="タグ" helperText="20文字まで" onChange={(e) => this.onChangeTag(e)} />
                </Grid>
                <Grid item xs={4}>
                    <TextField id="ulviewpass" label="閲覧パスワード" helperText="英数字記号20文字まで" type="text" onChange={(e) => this.onChangeViewpass(e)} />
                </Grid>
                <Grid item xs={4}>
                    <TextField id="uldelpass" label="削除パスワード" helperText="英数字記号20文字まで" type="text" onChange={(e) => this.onChangeDelpass(e)} />
                </Grid>
                <Grid item xs={4} />
                <Grid item xs={4} >
                    {this.formSubmit()}
                </Grid>
                <Grid item xs={4}>
                    {this.displayProgress()}
                </Grid>
                {this.renderSnackbars()}
            </Grid>
        );
    }

    renderCaptionUpload() {
        return(
        <Typography variant="caption" sx={{color: 'text.secondary'}}>
            ファイルと作者名は必須項目です。アップロードできるファイルサイズは100MB以下です。<br />
            閲覧パスワードで保護された投稿はパスワードを入力しなければ閲覧できません。<br />
            ただし、編集者は保護のある投稿もダウンロードする事ができます。<br />
            削除パスワードが空の投稿は、削除キーが空白でも削除できるようになります。<br />
            対応ファイル形式：[jpg, jpeg, png, gif, bmp, wbmp, xbm, zip, lzh, txt, doc, docx, pptx, pdf, mp3, wav, flac, mid, mp4, psd, heic]
        </Typography>
        );
    }

    render() {
        return (
            <Box>
            <Paper elevation={2} sx={{p: 2}}>
                {this.formInputs()}
            </Paper>
            {this.renderCaptionUpload()}
            </Box>
        );
    }
}

class TableTime extends React.Component {
    //utility class ...
    constructor(props) {
        super(props);
    }

    convertUnixtime(i) {
        const date = new Date(i * 1000);
        const ret = date.toLocaleDateString() + ' ' + date.toLocaleTimeString();
        return(ret);
    }

    convertUnixtimeLocalShort(i) {
        const date = new Date(i * 1000);
        const ret = date.toLocaleDateString('ja-JP', {year: '2-digit', month: '2-digit', day: '2-digit'}) + ' ' + date.toLocaleTimeString('ja-JP', {hour: '2-digit', minute:'2-digit'});
        return(ret);
    }

    getExt(fname) {
        const arr = fname.split('.');
        const lastarr = arr.length - 1;
        return(arr[lastarr]);
    }

    render() {
        if (this.props.time === undefined) {
            return(false);
        }
        if (this.props.mode != undefined) {
            return(this.convertUnixtimeLocalShort(this.props.time));
        } else {
            return(this.convertUnixtime(this.props.time));
        }
    }
}

class ItemList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            columns: [
                { field: 'id', headerName: 'ID', width: 70 },
                { field: 'title', headerName: 'タイトル', width: 70 },
                { field: 'author', headerName: '作者名', width: 70 },
                { field: 'date', headerName: '投稿日時', width: 70 },
                { field: 'action', headerName: '変更', width: 70,
                  disableClickEventBubblink: true,
                  renderCell: (params) => <Button variant="contained" onClick={() => this.onClick({params})}>変更</Button>
                },
            ],
            items: Array(1).fill({title: '読込中…'}),
            currentimg: {},
            itempage: 9,
            totalpage: 0,
            nextauth: false,
            open: false,
            viewpass: '',
            currentid: 0,
            opennotice: false,
            opensecond: false,
            defaultpage: this.props.currentPage,
            datasave: this.props.datasave,
            fade: true,
        };
        console.log('ItemList constructed');
    }

    componentDidMount() {
        var resp = Array(Array("abc", "def", "ghi"), "d");
        // ... https://stackoverflow.com/questions/41194866/how-to-set-state-of-response-from-axios-in-react
        var self = this;
        console.log(this.props.currentPage);
        this.setCurrentPage(this.props.currentPage);
        const paramsid = new URLSearchParams();
        paramsid.append('num', 'last');
        // get total id
        axios.post(this.props.path + 'list/json', paramsid)
            .then(function (response) {
                const resp = response.data;
                const totalpage = Math.ceil(resp / self.state.itempage);
                console.log('componentDidmount() totalpage: ' + totalpage);
                self.setState({totalpage: totalpage});
            }).catch(function (error) {
                console.log(error);
            });
    }

    updateList(from, num) {
        var self = this;
        const params = new URLSearchParams();
        params.append('from', from);
        params.append('num', num);
        // get list
        axios.post(this.props.path + 'list/json', params)
            .then(function (response) {
                const resp = Object.values(response.data);
                console.log(resp);
                if (resp[0] == "-") {
                    self.setState({items: Array(1).fill({title: 'まだないです。'})});
                } else {
                    self.setState({items: resp});
                }
            }).catch(function (error) {
                console.log(error);
            });
    }

    getImageDetail(i) {
        var currentimg = {};
        var self = this;
        var nextauth = false;
        const params = new URLSearchParams();
        params.append('viewpass', self.state.viewpass);
        axios.post(this.props.path + 'view/json/' + i, params)
            .then(function (response) {
                if (response.data.code > -1) {
                    currentimg.id = i;
                    currentimg.title = response.data.title;
                    currentimg.author = response.data.author;
                    currentimg.tag = response.data.tag;
                    currentimg.comment = response.data.comment;
                    currentimg.comments = response.data.comments;
                    currentimg.fname = response.data.fname;
                    currentimg.md5 = response.data.md5;
                    currentimg.ultime = response.data.ultime;
                    currentimg.code = response.data.code;
                    currentimg.viewpass = self.state.viewpass;
                    currentimg.filesize = response.data.filesize;
                    currentimg.datasave = self.state.datasave;
                    nextauth = false;
                    console.log("nextauth false");
                } else {
                    currentimg.id = i;
                    currentimg.code = response.data.code;
                    nextauth = true;
                    console.log("nextauth true");
                }
                console.log('getImageDetail() nextauth: ' + nextauth);
                self.setState({currentid: i});
                self.props.setCurrentImg(currentimg);
                console.log(currentimg);
            }).then(function () {
                console.log("Entering page / auth routine ...");
                if (nextauth == true) {
                    self.openPasswordDialog();
                    console.log("password required ...");
                } else {
                    self.setState({fade: false});
                    console.log("page move ...");
                }
            }).catch(function (error) {
                console.log(error);
            });
    }

    openPasswordDialog() {
        if (this.state.opensecond) {
            this.setState({opennotice: true});
        }
        this.setState({open: true});
    }

    closePasswordDialog(i, j) {
        if (j == 'Enter' || j == null) {
            this.setState({open: false});
            console.log(j);
            if (i) {
                this.getImageDetail(this.state.currentid);
                this.setState({opensecond: true});
            } else {
                this.setState({opensecond: false});
            }
        }
    }

    viewImage(i) {
        if (i !== undefined) {
            this.getImageDetail(i);
        }
    }

    setCurrentPage(i) {
        var from = (i * this.state.itempage) - this.state.itempage;
        this.updateList(from, this.state.itempage);
        this.props.setCurrentPage(i);
    }

    setViewpass(i) {
        this.setState({viewpass: i.target.value});
    }

    handleNoticeClose() {
        this.setState({opennotice: false});
    }

    handleDatasaveChange() {
        if (this.state.datasave) {
            this.setState({datasave: false});
            this.props.setDatasave(false);
        } else {
            this.setState({datasave: true});
            this.props.setDatasave(true);
        }
    }

    renderDatasaveSwitch() {
        return(
            <Switch onChange={() => this.handleDatasaveChange()}/>
        );
    }
    
    renderTable() {
        return(
            <div style={{ width: '100%' }}>
            <Box display="flex" justifyContent="center" alignItems="center">
                <Pagination count={this.state.totalpage} defaultPage={this.state.defaultpage} onChange={(e, page) => this.setCurrentPage(page)} />
            </Box>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>タイトル</TableCell>
                            <TableCell align="right">作者名</TableCell>
                            <TableCell align="right">タグ</TableCell>
                            <TableCell align="right">投稿日時</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                    {this.state.items.map((row, index) => 
                        <TableRow
                        key={index}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                            <TableCell component="th" scope="row">
                            <Button variant="text" color="secondary" onClick={() => this.viewImage(row.id)} style={{textTransform: 'none'}}>{row.title}</Button>
                            </TableCell>
                            <TableCell align="right">{row.author}</TableCell>
                            <TableCell align="right">{row.tag}</TableCell>
                            <TableCell align="right"><TableTime time={row.ultime} mode={1} /></TableCell>
                        </TableRow>
                    )}
                    </TableBody>
                </Table>
            </TableContainer>
            <Box display="flex" justifyContent="right" alignItems="right">
                <FormGroup>
                <FormControlLabel control={<Switch checked={this.state.datasave} onChange={() => this.handleDatasaveChange()} />} label="プレビューを読み込まない" />
                </FormGroup>
            </Box>
            <Dialog open={this.state.open} onClose={() => this.closePasswordDialog(false, null)} onKeyDown={(e) => this.closePasswordDialog(true, e.key)}>
                <DialogTitle>パスワードを入力</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        パスワードが必要です。
                    </DialogContentText>
                    <TextField autoFocus id="password" type="text" fullWidth onChange={(e) => this.setViewpass(e)} />
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.closePasswordDialog(true, null)}>送信</Button>
                </DialogActions>
            </Dialog>
            <Snackbar open={this.state.opennotice} autoHideDuration={12000} onClose={() => this.handleNoticeClose()}
                      anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
                <Alert onClose={() => this.handleNoticeClose()} severity="error">
                <AlertTitle>エラー</AlertTitle>
                パスワードが違います。
                </Alert>
            </Snackbar>
            </div>
        );
    }

    fadeOut() {
        this.setState({fade: false});
    }

    endTransition() {
        if (this.state.fade == false) {
            this.props.setPage(2);
        }
    }

    render() {
        return(
            <Container>
                <Fade in={this.state.fade} addEndListener={(node, done) => {node.addEventListener('transitionend', (e) => {this.endTransition(e), done(e)}, false);}}>
                    {this.renderTable()}
                </Fade>
            </Container>
        );
    }
}

class ViewImage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: false,
            image: '',
            load: false,
            delpass: '',
            openerror: false,
            opennotice: false,
            comments: '',
            commentsauthor: '',
            opencommenterror: false,
            opencommentnotice: false,
            fade: true,
        };
        this.tabletime = new TableTime();
    }

    handleChange() {
        if (this.state.checked) {
            this.setState({checked: false});
        } else {
            this.setState({checked: true});
        }
    }

    displayAdditional() {
        const util = new TableTime;
        const src = this.props.path + 'view/' + this.props.currentImg.id + '.' + util.getExt(this.props.currentImg.fname);

        return(
            <div>
                <Collapse in={this.state.checked}>
                    <Divider />
                    <ListItem>
                        <ListItemText primary={this.props.currentImg.fname} secondary="元のファイル名" />
                    </ListItem>
                    <Divider />
                    <ListItem>
                        <ListItemText primary={Math.round(this.props.currentImg.filesize / 1000) + 'KB'} secondary="ファイルサイズ" />
                    </ListItem>
                    <Divider />
                    <ListItem>
                        <ListItemText primary={this.props.currentImg.md5} secondary="MD5ハッシュ" />
                    </ListItem>
                    <Divider />
                    <ListItem button component="a" href={src}>
                        <ListItemText primary={this.props.currentImg.id} secondary="ID/直接リンク" />
                    </ListItem>
                </Collapse>
            </div>
        );
    }

    onClickDownload(i) {
        const util = new TableTime;
        const src = this.props.path + 'view/' + i + '.' + util.getExt(this.props.currentImg.fname);
        return(
        <form action={src} method="POST" target="_blank">
            <input type="hidden" name="viewpass" value={this.props.currentImg.viewpass} />
            <Button variant="contained" type="submit">ダウンロード</Button>
        </form>
        );
    }

    renderInfo() {
        return(
            <Container>
                <Grid container spacing={2}>
                    <Grid item xs={9}>
                        <List>
                            <ListItem>
                                <ListItemText primary={this.props.currentImg.title} secondary="タイトル" />
                            </ListItem>
                            <Divider />
                            <ListItem>
                                <ListItemText primary={this.props.currentImg.author} secondary="作者名" />
                            </ListItem>
                            <Divider />
                            <ListItem>
                                <ListItemText primary={this.tabletime.convertUnixtime(this.props.currentImg.ultime)} secondary="投稿日時" />
                            </ListItem>
                            <Divider />
                            <ListItem>
                                <ListItemText primary={this.props.currentImg.comment ? this.props.currentImg.comment  : "（未設定）"} secondary="投稿者コメント" />
                            </ListItem>
                            <Divider />
                            <ListItem>
                                <ListItemText primary={this.props.currentImg.tag ? this.props.currentImg.tag : "（未設定）"} secondary="タグ" />
                            </ListItem>
                            {this.displayAdditional()}
                        </List>
                    </Grid>
                    <Grid item xs={3}>
                    <Box>
                        {this.onClickDownload(this.props.currentImg.id)}
                        詳細を表示<br />
                        <Switch onChange={() => this.handleChange()}/>
                        </Box>
                    </Grid>
                </Grid>
            </Container>
        ); 
    }

    renderComments(i) {
        if (this.props.currentImg.comments != undefined) {
            return (
            <Container>
                <Chip label="コメント" />
                <List>
                {Object.values(this.props.currentImg.comments).map((row, index) => 
                <div>
                    <ListItem>
                        <ListItemText primary={(this.props.currentImg.comments.length - index) + '. ' + row.comment} secondary={row.author + " " + this.tabletime.convertUnixtimeLocalShort(row.modtime)} />
                    </ListItem>
                    <Divider />
                </div>
                )}
                </List>
            </Container>
            );
        } else {
            return false;
        }
    }

    renderImage(id, pass) {
        const util = new TableTime;
        const previewimg = ['jpg', 'jpeg', 'png', 'gif'];
        const previewaud = ['mp3', 'flac'];
        const previewvid = ['mp4'];
        const previewpdf = ['pdf'];
        if (this.props.currentImg.datasave) {
            return(<Typography>プレビューはありません。</Typography>);
        }
        if (previewimg.includes(util.getExt(this.props.currentImg.fname))) {
            if (this.state.load == false) {
                const src = this.props.path + 'view/' + id + '.b64';
                var self = this;
                const params = new URLSearchParams();
                params.append('viewpass', pass);
                axios.post(src, params)
                .then(function (response) {
                    const base64 = response.data;
                    self.setState({image: base64});
                }).catch(function (error) {
                    console.log(error);
                });
                this.setState({load: true});
            }
            return(<img src={'data:image/' + this.getExt() + ';base64,' + this.state.image} />);
        } else if (this.props.currentImg.viewpass == "" && previewaud.includes(util.getExt(this.props.currentImg.fname))) {
            return(<audio controls src={this.props.path + 'view/' + id + '.' + util.getExt(this.props.currentImg.fname)} />);
        } else if (this.props.currentImg.viewpass == "" && previewpdf.includes(util.getExt(this.props.currentImg.fname))) {
            return(<iframe src={this.props.path + 'view/' + id + '.' + util.getExt(this.props.currentImg.fname)} width="640" height="480" />);
        } else if (this.props.currentImg.viewpass == "" && previewvid.includes(util.getExt(this.props.currentImg.fname))) {
            return(<video controls src={this.props.path + 'view/' + id + '.' + util.getExt(this.props.currentImg.fname)} />);
        } else {
            return(<Typography>プレビューはありません。</Typography>);
        }
    }

    getExt() {
        const fname = this.props.currentImg.fname;
        const arr = fname.split('.');
        const lastarr = arr.length - 1;
        console.log(arr[lastarr]);
        return(arr[lastarr]);
    }

    setDelpass(e) {
        this.setState({delpass: e.target.value});
    }

    submitDelete(id) {
        var self = this;
        const params = new URLSearchParams();
        params.append('delpass', this.state.delpass);
        params.append('json', true);
        axios.post(this.props.path + 'view/' + id, params)
            .then(function (response) {
                const resp = response.data;
                console.log(resp);
                if (resp) {
                    self.setState({opennotice: true});
                } else {
                    self.setState({openerror: true});
                }
            }).catch(function (error) {
                console.log(error);
            });
    }

    handleErrorClose() {
        this.setState({openerror: false});
    }

    handleNoticeSnackClose() {
        this.setState({opennotice: false});
        this.props.setPage(1);
    }

    renderDelete(id) {
        return(
            <Container>
            削除パスワード
            <TextField autoFocus id="" type="text" onChange={(e) => this.setDelpass(e)} />
            <Button variant="contained" onClick={() => this.submitDelete(id)}>送信</Button>
            <Snackbar open={this.state.openerror} autoHideDuration={6000} onClose={() => this.handleErrorClose()}
                      anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
                <Alert onClose={() => this.handleErrorClose()} severity="error">
                <AlertTitle>エラー</AlertTitle>
                パスワードが違います。
                </Alert>
            </Snackbar>
            <Snackbar open={this.state.opennotice} autoHideDuration={6000} onClose={() => this.handleNoticeSnackClose()}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
                <Alert onClose={() => this.handleNoticeSnackClose()} severity="success">
                <AlertTitle>削除に成功しました。</AlertTitle>
                {this.state.noticemessage}
            </Alert>
            </Snackbar>
            </Container>
        );
    }

    setComments(i) {
        this.setState({comments: i.target.value});
    }

    setCommentsAuthor(i) {
        this.setState({commentsauthor: i.target.value});
    }

    submitComments(id, key) {
        if (key == 'Enter' || key == null) {
        var self = this;
        const params = new URLSearchParams();
        params.append('comment', this.state.comments);
        params.append('author', this.state.commentsauthor);
        params.append('json', true);
        axios.post(this.props.path + 'view/' + id, params)
            .then(function (response) {
                const resp = response.data;
                console.log(resp);
                if (resp == 1) {
                    self.setState({opencommentnotice: true});
                } else {
                    self.setState({opencommenterror: true});
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    }

    handleErrorCommentClose() {
        this.setState({opencommenterror: false});
    }

    handleNoticeCommentSnackClose() {
        this.setState({opencommentnotice: false});
    }

    renderCommentForm(id) {
        return(
            <Container>
                コメント
                <TextField autoFocus id="comment" type="text" onChange={(e) => this.setComments(e)} />
                名前
                <TextField autoFocus id="name" type="text" onChange={(e) => this.setCommentsAuthor(e)} onKeyDown={(e) => this.submitComments(id, e.key)} />
                <Button variant="contained" onClick={() => this.submitComments(id)}>送信</Button>
                <Snackbar open={this.state.opencommenterror} autoHideDuration={6000} onClose={() => this.handleErrorCommentClose()}
                      anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
                <Alert onClose={() => this.handleErrorCommentClose()} severity="error">
                <AlertTitle>エラー</AlertTitle>
                コメントの投稿に失敗しました。
                </Alert>
                </Snackbar>
                <Snackbar open={this.state.opencommentnotice} autoHideDuration={6000} onClose={() => this.handleNoticeCommentSnackClose()}
                anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
                    <Alert onClose={() => this.handleNoticeCommentSnackClose()} severity="success">
                    <AlertTitle>コメントの投稿に成功しました。</AlertTitle>
                    {this.state.noticemessage}
                </Alert>
                </Snackbar>
            </Container>
        );
    }

    fadeOut() {
        this.setState({fade: false});
    }

    endTransition() {
        if (this.state.fade == false) {
            this.props.setPage(1);
        }
    }

    renderView(i) {
        return(
            <Box>
                <Fade in={this.state.fade} addEndListener={(node, done) => {node.addEventListener('transitionend', (e) => {this.endTransition(e), done(e)}, false);}}>
                    <Paper elevation={2} sx={{p: 2}}>
                    <Button onClick={() => this.fadeOut()}>戻る</Button>
                    <Divider />
                    <Box display="flex" justifyContent="center" alignItems="center">
                        {this.renderImage(i.id, i.viewpass)}
                    </Box>
                    <Divider />
                    {this.renderInfo()}
                    <Divider />
                    {this.renderComments(this.props.currentImg.comments)}
                    <Divider />
                    {this.renderCommentForm(i.id)}
                    <Divider />
                    {this.renderDelete(i.id)}
                    <Divider />
                    <Button onClick={() => this.fadeOut()}>戻る</Button>
                    </Paper>
                </Fade>
            </Box>
        );

    }

    render() {
        return(<Container>{this.renderView(this.props.currentImg)}</Container>);
    }
}

class TagSave extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tag: '',
            pass: '',
            openerror: false,
            opennotice: false,
            succeed: false,
        };
    }

    componentDidMount() {
        console.log('TagSave: componentDidMount()');
    }

    getTagArchive() {
        var self = this;
        const params = new URLSearchParams();
        console.log(this.state.tag);
        console.log(this.state.pass);
        params.append('ziptag', this.state.tag);
        params.append('pass', this.state.pass);
        params.append('json', true);
        axios.post(this.props.path + 'view/zip', params)
        .then(function (response) {
            const resp = response.data;
            console.log(resp);
            if (resp == 1) {
                self.setState({opennotice: true});
                self.setState({succeed: true});
            } else {
                self.setState({openerror: true});
            }
        }).catch(function (error) {
            console.log(error);
        });
    }

    setTag(i) {
        this.setState({tag: i.target.value});
    }

    setTagPass(i) {
        this.setState({pass: i.target.value});
    }

    onClickTagDownload() {
        const src = this.props.path + 'zip';
        return(
        <form action={src} method="POST">
            <input type="hidden" name="ziptag" value={this.state.tag} />
            <input type="hidden" name="pass" value={this.state.pass} />
            <Button variant="contained" type="submit">ダウンロード</Button>
        </form>
        );
    }

    renderPostForm() {
        return(
            <Grid container spacing={2}>
                <Grid item xs={4}>
                    <TextField id="tag" type="text" label="タグ" helperText="一括保存するタグ" fullWidth onChange={(e) => this.setTag(e)} />
                </Grid>
                <Grid item xs={4}>
                    <TextField id="password" type="text" label="パスワード" fullWidth helperText="タグに設定されたパスワード" onChange={(e) => this.setTagPass(e)} />
                </Grid>
                <Grid item xs={4}>
                </Grid>
                <Grid item xs={2}>
                <Button variant="contained" onClick={() => this.getTagArchive()}>送信</Button>
                </Grid>
                <Grid item xs={4}>
                    {this.renderButtonDownload()}
                </Grid>
            </Grid>
        );
    }

    handleErrorClose() {
        this.setState({openerror: false});
    }

    handleNoticeSnackClose() {
        this.setState({opennotice: false});
    }

    renderSnackbars() {
        return(
            <Container>
            <Snackbar open={this.state.openerror} autoHideDuration={6000} onClose={() => this.handleErrorClose()}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
                <Alert onClose={() => this.handleErrorClose()} severity="error">
                <AlertTitle>エラー</AlertTitle>
                パスワード、またはタグが違います。
                </Alert>
            </Snackbar>
            <Snackbar open={this.state.opennotice} autoHideDuration={6000} onClose={() => this.handleNoticeSnackClose()}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
                <Alert onClose={() => this.handleNoticeSnackClose()} severity="success">
                <AlertTitle>zipファイルが生成されました。</AlertTitle>
                {this.state.noticemessage}
                </Alert>
            </Snackbar>
            </Container>
        );
    }

    renderCaptionDownload() {
        return(
        <Typography variant="caption" sx={{color: 'text.secondary'}}>
            管理者が発行したタグのパスワードを使用し、タグに属する投稿を一括で保存できます。<br />
            アーカイブには、それぞれの投稿のタイトル、コメント等が記載されたCSVファイルが入っています。<br />
            CSVファイルの閲覧には<a href="https://csviewer.com/" target="_blank">CSViewer</a>を推奨します。
        </Typography>
        );
    }

    renderButtonDownload() {
        if (this.state.succeed) {
            return(this.onClickTagDownload());
        } else {
            return(<Button variant="contained" disabled>ダウンロード</Button>);
        }
    }

    render() {
        return(
        <Container>
            <Paper elevation={2} sx={{p: 2}}>
            {this.renderPostForm()}
            </Paper>
            {this.renderCaptionDownload()}
            {this.renderSnackbars()}
        </Container>
        );
    }
}

class UserPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openerror: false,
            opennotice: false,
            masterpass: '',
            tag: '',
            tagpass: '',
            info: '',
            until: 0,
            year: 0,
            month: 13,
            day: 0
        };
    }

    componentDidMount() {
        console.log('UserPage: componentDidMount()');
    }

    setMasterPass(i) {
        this.setState({masterpass: i.target.value});
    }

    setTag(i) {
        this.setState({tag: i.target.value});
    }

    setTagPass(i) {
        this.setState({tagpass: i.target.value});
    }

    setInfo(i) {
        this.setState({info: i.target.value});
    }

    convertTimeUnix(year, month, day) {
        const datetime = new Date(year, month, day, 23, 59, 59);
        const unixtime = datetime.getTime() / 1000;
        return(unixtime);
    }

    updateUntil(year, month, day) {
        if (year == 0 || month == 13 || day == 0) {
            this.setState({until: 0});
        } else {
            const unixtime = this.convertTimeUnix(year, month, day);
            console.log(unixtime);
            this.setState({until: unixtime});
        }
    }

    setUntilYear(i) {
        this.setState({year: i.target.value});
        this.updateUntil(i.target.value, this.state.month, this.state.day);
    }

    setUntilMonth(i) {
        this.setState({month: i.target.value});
        this.updateUntil(this.state.year, i.target.value, this.state.day);
    }

    setUntilDay(i) {
        this.setState({day: i.target.value});
        this.updateUntil(this.state.year, this.state.month, i.target.value);
    }

    postRequest() {
        console.log(this.state.masterpass);
        console.log(this.state.tag);
        console.log(this.state.tagpass);
        console.log(this.state.info);
        console.log(this.state.until);
        var self = this;
        const params = new URLSearchParams();
        params.append('masterpass', this.state.masterpass);
        params.append('tag', this.state.tag);
        params.append('pass', this.state.tagpass);
        params.append('info', this.state.info);
        params.append('until', this.state.until);
        params.append('json', true);
        axios.post(this.props.path + 'user/tag', params)
        .then(function (response) {
            const resp = response.data;
            console.log(resp);
            if (resp == 1) {
                self.setState({opennotice: true});
            } else {
                self.setState({openerror: true});
            }
        }).catch(function (error) {
            console.log(error);
        });
        return(false);
    }

    renderPostForm() {
        return(
            <Container>
                <TextField id="masterpass" type="text" label="マスターパスワード" helperText="管理者用パスワード" onChange={(e) => this.setMasterPass(e)} />
                <TextField id="tag" type="text" label="タグ" onChange={(e) => this.setTag(e)} />
                <TextField id="pass" type="text" label="パスワード" helperText="タグに設定するパスワード" onChange={(e) => this.setTagPass(e)} />
                <TextField id="info" type="text" label="詳細情報" helperText="タグの詳細" onChange={(e) => this.setInfo(e)} />
                <FormControl>
                <InputLabel id="untilyear">年</InputLabel>
                <Select id="untilyear" label="年" onChange={(e) => this.setUntilYear(e)}>
                    <MenuItem value={0}>なし</MenuItem>
                    <MenuItem value={2022}>2022</MenuItem>
                    <MenuItem value={2023}>2023</MenuItem>
                    <MenuItem value={2024}>2024</MenuItem>
                </Select>
                <FormHelperText id="untilhelper">タグの有効期限</FormHelperText>
                </FormControl>
                <FormControl>
                <InputLabel id="untilmonth">月</InputLabel>
                <Select id="untilmonth" label="月" onChange={(e) => this.setUntilMonth(e)}>
                    <MenuItem value={13}>なし</MenuItem>
                    <MenuItem value={0}>1</MenuItem>
                    <MenuItem value={1}>2</MenuItem>
                    <MenuItem value={2}>3</MenuItem>
                    <MenuItem value={3}>4</MenuItem>
                    <MenuItem value={4}>5</MenuItem>
                    <MenuItem value={5}>6</MenuItem>
                    <MenuItem value={6}>7</MenuItem>
                    <MenuItem value={7}>8</MenuItem>
                    <MenuItem value={8}>9</MenuItem>
                    <MenuItem value={9}>10</MenuItem>
                    <MenuItem value={10}>11</MenuItem>
                    <MenuItem value={11}>12</MenuItem>
                </Select>
                </FormControl>
                <FormControl>
                <InputLabel id="untilday">日</InputLabel>
                <Select id="untilday" label="日" onChange={(e) => this.setUntilDay(e)}>
                    <MenuItem value={0}>なし</MenuItem>
                    <MenuItem value={1}>1</MenuItem>
                    <MenuItem value={2}>2</MenuItem>
                    <MenuItem value={3}>3</MenuItem>
                    <MenuItem value={4}>4</MenuItem>
                    <MenuItem value={5}>5</MenuItem>
                    <MenuItem value={6}>6</MenuItem>
                    <MenuItem value={7}>7</MenuItem>
                    <MenuItem value={8}>8</MenuItem>
                    <MenuItem value={9}>9</MenuItem>
                    <MenuItem value={10}>10</MenuItem>
                    <MenuItem value={11}>11</MenuItem>
                    <MenuItem value={12}>12</MenuItem>
                    <MenuItem value={13}>13</MenuItem>
                    <MenuItem value={14}>14</MenuItem>
                    <MenuItem value={15}>15</MenuItem>
                    <MenuItem value={16}>16</MenuItem>
                    <MenuItem value={17}>17</MenuItem>
                    <MenuItem value={18}>18</MenuItem>
                    <MenuItem value={19}>19</MenuItem>
                    <MenuItem value={20}>20</MenuItem>
                    <MenuItem value={21}>21</MenuItem>
                    <MenuItem value={22}>22</MenuItem>
                    <MenuItem value={23}>23</MenuItem>
                    <MenuItem value={24}>24</MenuItem>
                    <MenuItem value={25}>25</MenuItem>
                    <MenuItem value={26}>26</MenuItem>
                    <MenuItem value={27}>27</MenuItem>
                    <MenuItem value={28}>28</MenuItem>
                    <MenuItem value={29}>29</MenuItem>
                    <MenuItem value={30}>30</MenuItem>
                    <MenuItem value={31}>31</MenuItem>
                </Select>
                </FormControl>
                <Button variant="contained" onClick={() => this.postRequest()}>送信</Button>
            </Container>
            );
    }

    handleErrorClose() {
        this.setState({openerror: false});
    }

    handleNoticeSnackClose() {
        this.setState({opennotice: false});
    }

    renderSnackbars() {
        return(
            <Container>
            <Snackbar open={this.state.openerror} autoHideDuration={6000} onClose={() => this.handleErrorClose()}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
                <Alert onClose={() => this.handleErrorClose()} severity="error">
                <AlertTitle>エラー</AlertTitle>
                マスターパスワードが違います。
                </Alert>
            </Snackbar>
            <Snackbar open={this.state.opennotice} autoHideDuration={6000} onClose={() => this.handleNoticeSnackClose()}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
                <Alert onClose={() => this.handleNoticeSnackClose()} severity="success">
                <AlertTitle>成功しました。</AlertTitle>
                {this.state.noticemessage}
                </Alert>
            </Snackbar>
            </Container>
        );
    }
    renderCaptionUsers() {
        return(
        <Typography variant="caption" sx={{color: 'text.secondary'}}>
            管理用のページです。
        </Typography>
        );
    }

    render() {
        return(
            <Container>
                <Paper elevation={2} sx={{p: 2}}>
                    {this.renderPostForm()}
                </Paper>
                {this.renderCaptionUsers()}
                {this.renderSnackbars()}
            </Container>
        );
    }
}

class Uler extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // page 1 = list, 2 = detail, 3 = tag, 4 = user, 5 = upload
            page: 1,
            currentimg: {},
            currentpage: 1,
            path: 'http://www.example.com/uler/',
            datasave: false,
            globalnotice: '',
            secondnotice: '',
        };
        this.setPage = this.setPage.bind(this);
        this.setCurrentImg = this.setCurrentImg.bind(this);
        this.setCurrentPage = this.setCurrentPage.bind(this);
        this.setDatasave = this.setDatasave.bind(this);
    }

    componentDidMount() {
        this.getGlobalnotice();
    }

    getProperties() {
        return 1;
    }

    setPage(i) {
        this.setState({page: i});
    }

    setCurrentImg(i) {
        this.setState({currentimg: i});
    }

    setCurrentPage(i) {
        this.setState({currentpage: i});
    }

    setDatasave(i) {
        this.setState({datasave: i});
    }

    getGlobalnotice() {
        var self = this;
        axios.get(this.state.path + 'global.txt')
        .then(function (response) {
            const resp = Object.values(response.data);
            console.log('globalnotice get');
            console.log(resp);
            const str = resp.join('');
            const strs = str.split('\n');
            self.setState({globalnotice: strs[0], secondnotice: strs[1]});
        }).catch(function (error) {
            console.log(error);
        });
    }

    renderGlobalNotice() {
        return(
            <Box>
                <Typography variant="body2">
                【お知らせ】<br />
                {this.state.globalnotice}<br />
                {this.state.secondnotice}
                </Typography>
            </Box>
        );
    }

    renderAppbar() {
        return (<AppBar position="static">
        <Toolbar>
            <Typography variant="h6" component="div" sx={{flexGrow: 1}}>Uler</Typography>
            <Button color="inherit" onClick={() => this.setPage(1)}>リスト</Button>
            <Button color="inherit" onClick={() => this.setPage(5)}>投稿する</Button>
            <Button color="inherit" onClick={() => this.setPage(3)}>一括保存</Button>
            <Button color="inherit" onClick={() => this.setPage(4)}>ユーザー</Button></Toolbar>
        </AppBar>);
    }

    renderFooter() {
        return(
            <Box sx={{m: 0.5}}>
            <Grid container justifyContent="flex-end">
                <Typography variant="caption" style={{ color: 'gray' }}>Uler v1.0</Typography>
            </Grid>
            </Box>
        );
    }

    render() {
        let nowpage;
        let pagedespriction;
        const theme = createTheme({
            palette: {
                mode: 'light',
                primary: {
                    light: '#ff6659',
                    main: '#d32f2f',
                    dark: '#9a0007',
                    contrastText: '#fff',
                },
                secondary: {
                    light: '#9a67ea',
                    main: '#1775d1',
                    dark: '#320b86',
                    contrastText: '#fff',
                },
                background: {
                    paper: '#fcfcfd',
                    default: '#f4f4f4',
                },
              }
        });
        if (this.state.page == 1) {
            nowpage = <ItemList path={this.state.path} 
                        setPage={this.setPage} setCurrentImg={this.setCurrentImg} 
                        setCurrentPage={this.setCurrentPage} 
                        currentPage={this.state.currentpage}
                        setDatasave={this.setDatasave}
                        datasave={this.state.datasave} />;
            pagedespriction = "リスト表示（新しい順）";
        } else if (this.state.page == 2) {
            nowpage = <ViewImage path={this.state.path} setPage={this.setPage} currentImg={this.state.currentimg} datasave={this.state.datasave} />;
            pagedespriction = "";
        } else if (this.state.page == 3) {
            nowpage = <TagSave path={this.state.path} />;
            pagedespriction = "一括保存";
        } else if (this.state.page == 4) {
            nowpage = <UserPage path={this.state.path} />;
            pagedespriction = "ユーザー";
        } else if (this.state.page == 5) {
            nowpage = <UploadForm path={this.state.path} />
            pagedespriction = "投稿する";
        }
        return (
            <ThemeProvider theme={theme}>
                <CssBaseline />
                {this.renderAppbar()}
                <Container>
                    
                    <Box sx={{p: 2}}>
                        <Paper elevation={2} sx={{p: 2}}>
                            {this.renderGlobalNotice()}
                        </Paper>
                        <Box sx={{p: 1}}>
                            <Typography variant="h6">{pagedespriction}</Typography>
                        </Box>
                    {nowpage}
                    </Box>
                    {this.renderFooter()}
                </Container>
                
            </ThemeProvider>
        );
    }
}

export default Uler;