<?php
/* Uler upload uler/up/ 22/11/03 */

function upcheck($filename, $author, $title, $comment, $tag, $delpass, $viewpass) {
    global $upfilenamelimit;
    global $upauthorlimit;
    global $uptitlelimit;
    global $upcommentlimit;
    global $allowext;

    if (isset($filename) === false or $filename == ""
        or strlen($filename) > $upfilenamelimit) {
        return -1;
    }
    $fileext = strtolower(Uldb::getExt($filename));
    if (in_array($fileext, $allowext) === false) {
        return -2;
    }
    if (isset($author) === false or $author == ""
        or mb_strlen($author) > $upauthorlimit) {
        return -3;
    }
    if (isset($title) === false or mb_strlen($title) > $uptitlelimit
        or $title == "empty") {
        return -4;
    }
    if (isset($comment) === false or mb_strlen($comment) > $upcommentlimit
        or $comment == "empty") {
        return -5;
    }
    if (isset($tag) === false or mb_strlen($tag) > 20 or $tag == "empty") {
        return -6;
    }
    if (isset($delpass) === false or ($delpass != "" and preg_match('/\A[a-zA-Z0-9!-\/:-@\[-~]{1,20}\z/', $delpass) === 0)) {
        return -7;
    }
    if (isset($viewpass) === false or ($viewpass != "" and preg_match('/\A[a-zA-Z0-9!-\/:-@\[-~]{1,20}\z/', $viewpass) === 0)) {
        return -8;
    }
    return 1;
}

function upthumb($filename, $dist) {
    $sizes = getimagesize($filename);
    if ($sizes[2] == 1) {
        $base = imagecreatefromgif($filename);
    } elseif ($sizes[2] == 2) {
        $base = imagecreatefromjpeg($filename);
    } elseif ($sizes[2] == 3) {
        $base = imagecreatefrompng($filename);
    } elseif ($sizes[2] == 6) {
        $base = imagecreatefrombmp($filename);
    } elseif ($sizes[2] == 15) {
        $base = imagecreatefromwbmp($filename);
    } elseif ($sizes[2] == 16) {
        $base = imagecreatefromxbm($filename);
    } else {
        return false;
    }
    $height = 200 * $sizes[1] / $sizes[0];
    $thumbnail = imagecreatetruecolor(200, $height);
    imagecopyresampled($thumbnail, $base, 0, 0, 0, 0, 200, $height, $sizes[0], $sizes[1]);
    imagejpeg($thumbnail, $dist, 85);
    return true;
}

function uphandle() {
    global $uptitlelimit;
    global $dbfilename;

    $ret = upcheck($_FILES['userfile']['name'], $_POST["author"], $_POST["title"], $_POST["comment"], $_POST["tag"], $_POST["delpass"], $_POST["viewpass"]);

    if ($ret < 0) {
        return $ret;
    }
    
    $fn = $_FILES['userfile']['name'];
    $fsrc = $_FILES['userfile']['tmp_name'];
    if ($_POST["title"] == "") {
        if (mb_strlen($_POST["title"]) > $uptitlelimit) {
            $ftitle = htmlspecialchars(substr($fn, 0, 29)."...");
        } else {
            $ftitle = htmlspecialchars($fn);
        }
    } else {
        $ftitle = htmlspecialchars($_POST["title"]);
    }
    $fauthor = htmlspecialchars($_POST["author"]);
    $fcomment = htmlspecialchars($_POST["comment"]);
    $tag = htmlspecialchars($_POST["tag"]);
    if ($_POST["viewpass"] != "") {
        $viewpass = password_hash($_POST["viewpass"], PASSWORD_BCRYPT);
    } else {
        $viewpass = "";
    }
    if ($_POST["delpass"] != "") {
        $delpass = password_hash($_POST["delpass"], PASSWORD_BCRYPT);
    } else {
        $delpass = "";
    }
    // unique is not uuid
    $unique = uniqid();
    $time = time();
    $authorid = substr($_SERVER["HTTP_CF_CONNECTING_IP"], -4);

    // file upload
    $fdist = "images/".$unique;
    if (move_uploaded_file($fsrc, $fdist) === false) {
        return -9;
    }
    upthumb($fdist, $fdist."_thumb");
    $md5 = md5_file($fdist);

    // db 
    $uldb = new Uldb($dbfilename);
    $uldb->connect();
    $id = $uldb->selectLastId() + 1;
    if ($uldb->insertImages($id, $fn, $time, $ftitle, $fauthor, $authorid, $fcomment, $tag, $unique, $viewpass, $delpass, $md5) === false) {
        return -10;
    }
    $uldb->updateLastId($id);
    $uldb = NULL;
    return 1;
}

function upmain() {
    global $upform;
    global $out;
    global $allowext;
    global $backto;
    global $headertitle;
    $backto = "../list/";
    $headertitle = "Uler upload";

    if (isset($_FILES['userfile'])) {
        if (isset($_POST['json']) === false) {
            $res = uphandle();
            if ($res > 0) {
                $json["res"] = $res;
                $json["Status"] = "Success";
                $out .= json_encode($json);
            } else {
                $json["res"] = $res;
                $json["Status"] = "Failure";
                $out .= json_encode($json);
            }
        } else {
            echo uphandle();
            exit();
        }
    } else {
        $out .= $upform;
        $out .= "<br>Allowed file extension:<br>\r\n";
        for ($i = 0; $i < count($allowext); $i++) {
            $out .= $allowext[$i].", ";
        }
    }
}