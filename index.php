<?php
/* Uler index.php uler/ 22/11/03 */

// load settings
require("ul-set.php");
require("ul-db.php");

global $scpath;

// for dev
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');

// output
$out = "";

if ($_GET["mode"] === "list") {
    require("ul-gal.php");
    if (isset($_GET["json"])) {
        galjson($_POST["from"], $_POST["num"]);
    } else {
        if (isset($_GET["page"])) {
            galmain($_GET["page"]);
        } else {
            galmain(0);
        }
    }
} elseif ($_GET["mode"] === "up") {
    require("ul-up.php");
    upmain();
} elseif ($_GET["mode"] === "view") {
    require("ul-view.php");
    viewmain();
} elseif ($_GET["mode"] === "user") {
    require("ul-user.php");
    usermain();
} elseif ($_GET["mode"] === "rss") {
    require("ul-rss.php");
    rssmain();
} else {
    $html .= "mode not valid, go to gal ...<br>\r\n";
    header('Location: '.$scpath.'gal/');
}

if ($_GET["mode"] != "view" and $_GET["mode"] != "rss") {
    $footer = str_replace("%back%", $backto, $footer);
    $header = str_replace("%title%", $headertitle, $header);
    $html = $header.$out.$footer;
    echo $html;
}