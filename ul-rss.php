<?php
/* Uler rss uler/rss/ 23/02/11 */

function rssmain() {
    global $scpath;
    global $dbfilename;

    $rss = new Ulrss($dbfilename, $scpath);
    if ($_GET["two"] != "") {
        header('Content-Type: text/plain');
        if ($_GET["two"] == "1") {
            echo $rss->generateSubject();
        } elseif ($_GET["two"] == "2") {
            echo $rss->generateDat($_GET["time"]);
        } elseif ($_GET["two"] == "3") {
            if ($_POST["bbs"] != "test") {
                return -1;
            }
            $time = $_POST["key"];
            if ($rss->postComment($time, $_POST["MESSAGE"], $_POST["FROM"])) {
                return 1;
            } else {
                return -1;
            }
        } else {
            echo $rss->generateSetting();
        }
    } else {
        header('Content-Type: application/xml');
        echo $rss->generateRSS();
    }
    return 1;
}

class Ulrss {
    /* class returns rss and more ... */

    private $dbaddr;
    private $scpath;

    public function __construct($dbaddr, $scpath) {
        $this->dbaddr = $dbaddr;
        $this->scpath = $scpath;
    }

    public function generateRSS() {
        $rss = new UlrssInternal($this->dbaddr, $this->scpath);
        $out = $rss->getRSS();
        return $out;
    }

    public function generateDat($time) {
        $two = new UlrssTwochannel($this->dbaddr, $this->scpath);
        $out = $two->getDat($time);
        return $out;
    }

    public function generateSubject() {
        $two = new UlrssTwochannel($this->dbaddr, $this->scpath);
        $out = $two->getSubject();
        return $out;
    }

    public function generateSetting() {
        $two = new UlrssTwochannel($this->dbaddr, $this->scpath);
        $out = $two->getSetting();
        return $out;
    }

    public function postComment($id, $comment, $name) {
        $two = new UlrssTwochannel($this->dbaddr, $this->scpath);
        $out = $two->writeComment($id, $comment, $name);
        return $out;
    }
}

class UlrssTwochannel {
    /* 2-channel related 23/02/11
       subject.txt, SETTING.TXT, logfile
       and bbs.cgi support */

    private $dbaddr;
    private $scpath;

    public function __construct($dbaddr, $scpath) {
        $this->dbaddr = $dbaddr;
        $this->scpath = $scpath;
    }

    private function getListfromDB() {
        $uldb = new Uldb($this->dbaddr);
        $uldb->connect();
        $list = $uldb->selectRssList(0, 9);
        return $list;
    }

    private function getPostfromDB($id) {
        $uldb = new Uldb($this->dbaddr);
        $uldb->connect();
        $response = $uldb->selectView($id);
        return $response;
    }

    private function getCommentsfromDB($id) {
        $uldb = new Uldb($this->dbaddr);
        $uldb->connect();
        $response = $uldb->selectViewComments($id);
        return $response;
    }

    public function writeComment($time, $comment, $name) {
        if ($time == "" or $comment == "") {
            return -1;
        }
        if (is_numeric($time) === false) {
            return -1;
        }
        if (mb_strlen($comment) > 250 or mb_strlen($name) > 50) {
            return -1;
        }
        $comment = mb_convert_encoding($comment, "UTF-8", "SJIS");
        $name = mb_convert_encoding($name, "UTF-8", "SJIS");
        $uldb = new Uldb($this->dbaddr);
        $uldb->connect();
        $id = $uldb->selectIdfromTime($time);
        if ($id === false) {
            return -1;
        }
        if ($uldb->insertComments($id, $comment, $name)) {
            return 1;
        }
        return -1;
    }

    public function getSetting() {
        $setting = "BBS_TITLE=Uler\n";
        return $setting;
    }

    public function getSubject() {
        $list = $this->getListfromDB();
        $resnumber = 1;
        $subject = "";
        for($i = 0; $i < count($list); $i++) {
            $subject .= $list[$i]["ultime"].".dat<>".$list[$i]["title"]." (".$resnumber.")\n";
        }
        $subject = mb_convert_encoding($subject, "SJIS", "UTF-8");
        return $subject;
    }

    public function getDat($time) {
        // return 2-channel dat
        $uldb = new Uldb($this->dbaddr);
        $uldb->connect();
        $id = $uldb->selectIdfromTime($time);
        $uldb = NULL;
        $post = $this->getPostfromDB($id);
        $comments = $this->getCommentsfromDB($id);
        if ($post["comment"] == "") {
            $post["comment"] = "まだない";
        }
        $str = mb_convert_encoding($post["author"], "SJIS", "UTF-8");
        $dat = $post["author"]."<><>".date(DATE_RFC2822, $post["ultime"])." ID:".$post["authorid"]."<>".$this->scpath."view/".$id.".jpg<br>".$post["comment"]."<>".$post["title"]."\n";
        if ($comments != "") {
            for($i = count($comments)-1; $i >= 0; $i--) {
                if ($comments[$i]["author"] == "") {
                    $comments[$i]["author"] = "名無しさん";
                }
                $dat .= $comments[$i]["author"]."<><>".date(DATE_RFC2822, $comments[$i]["modtime"])."<>".$comments[$i]["comment"]."<>\n";
            }
        }
        $dat = mb_convert_encoding($dat, "SJIS", "UTF-8");
        return $dat;
    }
}

class UlrssInternal {
    /* rss related 23/02/11
       channel has title, link, description, lang
       each items have title, link, description, pubDate */

    private $dbaddr;
    private $scpath;

    public function __construct($dbaddr, $scpath) {
        $this->dbaddr = $dbaddr;
        $this->scpath = $scpath;
    }

    private function getTenLatestPostFromDB() {
        // return = assoc array, 10 latest posts
        $uldb = new Uldb($this->dbaddr);
        $uldb->connect();
        $list = $uldb->selectRssList(0, 9);
        return $list;
    }

    private function generateChannelInfo($list) {
        $out = "<title>Uler</title>\r\n";
        $out .= "<link>".$this->scpath."</link>\r\n";
        $out .= "<description>Images from internet users ...</description>\r\n";
        $out .= "<language>ja</language>\r\n";
        return $out;
    }

    private function generateItems($list) {
        $out = "";
        for ($i = 0; $i < count($list); $i++) {
            if ($list[$i]["viewpass"] != "") {
                continue;
            }
            if ($list[$i]["comment"] == "") {
                $list[$i]["comment"] = "Description is not available.";
            }

            // build a rss ...
            $out .= "<item>\r\n";
            $out .= "<title>".$list[$i]["title"]."</title>\r\n";
            $out .= "<description>".$list[$i]["comment"]."</description>\r\n";
            $out .= "<link>".$this->scpath."view/".$list[$i]["id"]."</link>\r\n";
            $out .= "<pubDate>".date(DATE_RFC2822, $list[$i]["ultime"])."</pubDate>\r\n";
            $out .= "</item>\r\n";
        }
        return $out;
    }

    public function getRSS() {
        //return = string, rss feed
        $list = $this->getTenLatestPostFromDB();
        $rss = "<rss version=\"0.92\">\r\n<channel>\r\n";
        $rss .= $this->generateChannelInfo($list);
        $rss .= $this->generateItems($list);
        $rss .= "</channel></rss>";
        return $rss;
    }
}