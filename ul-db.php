<?php
/* Uler db 22/11/03 */

class Uldb {
    private $dbfilename;
    private $pdo;

    public function __construct($name) {
        if (!empty($name) and is_string($name)) {
            $this->dbfilename = $name;
        }
    }

    public function connect() {
        $this->pdo = new PDO('sqlite:'.$this->dbfilename);
        $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            // table init
            $this->pdo->exec("CREATE TABLE IF NOT EXISTS images (id INT PRIMARY KEY, fname TEXT, ultime INT, modtime INT, title TEXT, author TEXT, authorid TEXT, comment TEXT, tag TEXT, uuid TEXT, viewpass TEXT, delpass TEXT, md5 TEXT)");
            $this->pdo->exec("CREATE TABLE IF NOT EXISTS posts (id INT PRIMARY KEY, license TEXT, latitude REAL, longitude REAL)");
            /*
            wiki table has those tables:
            id=id of article, aname=name or title, ctime=created time, modtime=modified time, wp=is write protected
            */
            $this->pdo->exec("CREATE TABLE IF NOT EXISTS wiki (id INT PRIMARY KEY, aname TEXT, ctime INT, modtime INT, wp INT)");
            /*
            wiki metadata, optional, id is same as wiki table
            categorys are below
            0=none/data, 1=item, 2=food, 3=book, 4=music, 5=game/software, 6=place, 7-127 is reserved, 128-254 is user defined
            */
            $this->pdo->exec("CREATE TABLE IF NOT EXISTS wikimeta (id INT, category INT, modtime INT, link TEXT, latitude REAL, longitude REAL, manufacturer TEXT, rate INT, price REAL, contentshort TEXT)");
            $this->pdo->exec("CREATE TABLE IF NOT EXISTS wikicont (wid INT, ctime INT, author TEXT, content TEXT)");
            $this->pdo->exec("CREATE TABLE IF NOT EXISTS comments (id INT PRIMARY KEY, comment TEXT, author TEXT, upoint INT, modtime INT, toid INT)");
            $this->pdo->exec("CREATE TABLE IF NOT EXISTS users (id INT PRIMARY KEY, uname TEXT, pass TEXT, addr TEXT, auth INT, lastlogin INT)");
            $this->pdo->exec("CREATE TABLE IF NOT EXISTS tags (tag TEXT PRIMARY KEY, auth TEXT, info TEXT, until INT)");
            $this->pdo->exec("INSERT OR IGNORE INTO users (id, uname, pass, addr, auth, lastlogin) VALUES (0, 'master', '".password_hash("password", PASSWORD_BCRYPT)."', 0, 5, 0)");
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Get wiki id from name
     * @param string $aname name of article
     * @return int id of article, if failed false
     */
    public function selectWikiArticleIDfromName($aname) {
        try {
            $sql = "SELECT id, aname FROM wiki WHERE aname = :aname";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':aname' => $aname);
            $stmt->execute($params);
            $res = $stmt->fetch();
            // if no record returns or id isn't integer(not possible...), failed
            if (!$res or $res["aname"] != $aname or !is_int($res["id"])) {
                return false;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
        return $res["id"];
    }

    /**
     * Get wiki name from id
     * @param int $id id of article
     * @return string name of article, if failed false
     */
    public function selectWikiArticleNamefromID($id) {
        try {
            $sql = "SELECT id, aname FROM wiki WHERE id = :id";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':id' => $id);
            $stmt->execute($params);
            $res = $stmt->fetch();
            // if no record returns or id isn't integer(not possible...), failed
            if ($res["id"] != $id) {
                return false;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
        return $res["aname"];
    }

    /**
     * Get current revision of the article
     * @param int $wid id of article
     * @return int current revision of the article
     */
    public function selectWikiArticleLatest($wid) {
        try {
            $sql = "SELECT wid, ctime FROM wikicont WHERE wid = :wid ORDER BY ctime DESC";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':wid' => $wid);
            $stmt->execute($params);
            $res = $stmt->fetch();
            // if no record returns or ctime isn't integer(not possible...), failed
            if (!$res or $res["wid"] != $wid or !is_int($res["ctime"])) {
                return false;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
        return $res["ctime"];
    }

    /**
     * search wiki db by name
     * @param string $name search string
     * @return array
     */
    public function selectWikiNameInclude($aname, $from, $num) {
        $i = 0;
        $sql = "SELECT * FROM wiki WHERE aname LIKE :aname LIMIT :num offset :from";
        $stmt = $this->pdo->prepare($sql);
        $params = array(':aname' => '%'.$aname."%", ':num' => $num, ':from' => $from);
        $stmt->execute($params);
        while($row = $stmt->fetch()) {
            $list[$i] = $row;
            $i++;
        }
        // if no record matches the query returns, failed
        if (!isset($list[0])) {
            return false;
        }
        return $list;
    }

    /**
     * search wiki db number by name
     * @param string $name search string
     * @return array
     */
    public function selectWikiNumberNameInclude($aname) {
        $sql = "SELECT count(id) FROM wiki WHERE aname LIKE :aname";
        $stmt = $this->pdo->prepare($sql);
        $params = array(':aname' => '%'.$aname."%");
        $stmt->execute($params);
        $row = $stmt->fetch();
        // if no record matches the query returns, failed
        if (!isset($row["count(id)"])) {
            return false;
        }
        return $row["count(id)"];
    }

    /**
     * select write protected articles
     * @return array
     */
    public function selectWikiListWriteProtected($from, $num) {
        $i = 0;
        $sql = "SELECT * FROM wiki WHERE wp = 1 ORDER BY modtime DESC LIMIT :num offset :from";
        $stmt = $this->pdo->prepare($sql);
        $params = array(':num' => $num, ':from' => $from);
        $stmt->execute($params);
        while($row = $stmt->fetch()) {
            $list[$i] = $row;
            $i++;
        }
        // if no record matches the query returns, failed
        if (!isset($list[0])) {
            return false;
        }
        return $list;
    }

    /**
     * select number of write protected articles
     * @return int
     */
    public function selectWikiNumberWriteProtected() {
        $sql = "SELECT count(id) FROM wiki WHERE wp = 1";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        // if no record matches the query returns, failed
        if (!isset($row["count(id)"])) {
            return false;
        }
        return $row["count(id)"];
    }

    /**
     * get total number of articles
     * @return int
     */
    public function selectWikiTotalArticleNumber() {
        $sql = "SELECT count(id) FROM wiki";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        // if no record matches the query returns, failed
        if (!isset($row["count(id)"])) {
            return false;
        }
        return $row["count(id)"];
    }

    /**
     * get total number of meta of that category
     * @param int $metacat metadata category
     * @return int
     */
    public function selectWikiTotalMetaNumber($category) {
        if ($category < 255) {
            $sql = "SELECT count(id) FROM wikimeta WHERE category = :category";
            $params = array(':category' => $category);
        } else {
            $sql = "SELECT count(id) FROM wikimeta";
            $params = NULL;
        }
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);
        $row = $stmt->fetch();
        // if no record matches the query returns, failed
        if (!isset($row["count(id)"])) {
            return false;
        }
        return $row["count(id)"];
    }

    /**
     * get last updated articles
     * @param int $from offset
     * @param int $num number of item
     * @return array
     */
    public function selectWikiListLatest($from, $num) {
        $i = 0;
        $sql = "SELECT * FROM wiki ORDER BY modtime DESC LIMIT :num offset :from";
        $stmt = $this->pdo->prepare($sql);
        $params = array(':num' => $num, ':from' => $from);
        $stmt->execute($params);
        while($row = $stmt->fetch()) {
            $list[$i] = $row;
            $i++;
        }
        // if no record matches the query returns, failed
        if (!isset($list[0])) {
            return false;
        }
        return $list;
    }

    /**
     * Get wiki article contents
     * @param int $id id of article
     * @param int $ctime created time of article
     * @return array array of the record, if failed false
     */
    public function selectWikiArticleContent($wid, $ctime) {
        try {
            $sql = "SELECT * FROM wikicont WHERE wid = :wid AND ctime = :ctime";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':wid' => $wid, ':ctime' => $ctime);
            $stmt->execute($params);
            $res = $stmt->fetch();
            // if no record matches the query returns, failed
            if ($res["wid"] != $wid or $res["ctime"] != $ctime) {
                return false;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
        return $res;
    }

    /**
     * Select last ID from wiki table
     * @return int last id
     */
    public function selectWikiLastID() {
        try {
            $sql = "SELECT MAX(id) FROM wiki";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_NUM);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $res[0];
    }

    /**
     * Insert new article
     * @param int $id id of article
     * @param string $name article name
     * @return bool
     */
    public function insertWikiArticle($id, $aname) {
        try {
            $sql = "INSERT INTO wiki (id, aname, ctime, modtime, wp) VALUES (:id, :aname, :ctime, :modtime, 0)";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':id' => $id, ':aname' => $aname, ':ctime' => time(), ':modtime' => time());
            $stmt->execute($params);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
        return true;
    }

    /**
     * delete (... just unlink) wiki article by id
     * @param int id
     * @return int
     */
    public function deleteWikiArticle($id) {
        try {
            $sql = "DELETE FROM wiki WHERE id = :id";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':id' => $id);
            $stmt->execute($params);
        } catch (Exception $e) {
            echo $e->getMessage();
            return -1;
        }
        return 0;
    }

    /**
     * update modtime on wiki article
     * @param int $id
     * @param int $modtime
     * @return bool
    */
    private function updateWikiTimeModified($id, $modtime) {
        try {
            $sql = "UPDATE wiki SET modtime = :modtime WHERE id = :id";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':modtime' => $modtime, ':id' => $id);
            $stmt->execute($params);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
        return true;
    }

    /**
     * get write protected flag
     * @param int $id article id
     * @return bool true=write-protected
     */
    public function selectWikiWriteProtected($id) {
        try {
            $sql = "SELECT wp FROM wiki WHERE id = :id";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':id' => $id);
            $stmt->execute($params);
            $res = $stmt->fetch();
            if ($res["wp"] == 1)
                return true;
        } catch (Exception $e) {
            echo $e->getMessage();
            return true;
        }
        return false;
    }

    /**
     * Insert new article content
     * @param int $wid id of article
     * @param string $author author / updator of this article content
     * @param string $content content of article
     * @return bool
     */
    public function insertWikiArticleContent($wid, $author, $content) {
        try {
            if ($this->selectWikiWriteProtected($wid))
                return false;
            $ctime = time();
            $sql = "INSERT INTO wikicont (wid, ctime, author, content) VALUES (:wid, :ctime, :author, :content)";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':wid' => $wid, ':ctime' => $ctime, ':author' => $author, ':content' => $content);
            $stmt->execute($params);
            $this->updateWikiTimeModified($wid, time());
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
        return true;
    }

    /**
     * Insert article metadata
     * @param int $id id of article
     * @param int $category category of article
     * @param int $modtime create or modify time
     * @param string $link image ID or URL
     * @param double $latitude
     * @param double $longitude
     * @param string $manufacturer
     * @param int $rate rate of that item, 1-5
     * @param string $contentshort short description of article
     * @return bool
     */
    public function insertWikiArticleMeta($id, $category, $modtime, $link, $latitude, $longitude, $manufacturer, $rate, $contentshort) {
        try {
            if ($this->selectWikiWriteProtected($id))
                return false;
            $sql = "INSERT INTO wikimeta (id, category, modtime, link, latitude, longitude, manufacturer, rate, contentshort) VALUES (:id, :category, :modtime, :link, :latitude, :longitude, :manufacturer, :rate, :contentshort)";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':id' => $id, ':category' => $category, ':modtime' => $modtime, ':link' => $link, ':latitude' => $latitude, ':longitude' => $longitude, ':manufacturer' => $manufacturer, ':rate' => $rate, ':contentshort' => $contentshort);
            $stmt->execute($params);
            $this->updateWikiTimeModified($id, time());
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
        return true;
    }

    /**
     * Update article metadata
     */
    public function updateWikiArticleMeta($id, $category, $modtime, $link, $latitude, $longitude, $manufacturer, $rate, $contentshort) {
        try {
            if ($this->selectWikiWriteProtected($id))
                return false;
            $sql = "UPDATE wikimeta SET category = :category, modtime = :modtime, link = :link, latitude = :latitude, longitude = :longitude, manufacturer = :manufacturer, rate = :rate, contentshort = :contentshort WHERE id = :id";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':id' => $id, ':category' => $category, ':modtime' => $modtime, ':link' => $link, ':latitude' => $latitude, ':longitude' => $longitude, ':manufacturer' => $manufacturer, ':rate' => $rate, ':contentshort' => $contentshort);
            $stmt->execute($params);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
        return true;
    }

    /**
     * select Wiki Meta
     * @param int $id id of article
     * @return array
     */
    public function selectWikiArticleMeta($id) {
        try {
            $sql = "SELECT * FROM wikimeta WHERE id = :id";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':id' => $id);
            $stmt->execute($params);
            $res = $stmt->fetch();
            // if no record matches the query returns, failed
            if (!$res or $res["id"] != $id) {
                return -1;
            }

        } catch (Exception $e) {
            echo $e->getMessage();
            return -1;
        }
        return $res;
    }

    /**
     * select Wiki meta list (latest update)
     * @param int $from offset
     * @param int $num number
     * @param int $category category number, all if 255
     * @return array
     */
    public function selectWikiArticleMetaList($from, $num, $category) {
        $i = 0;
        $params = array(':num' => $num, ':from' => $from);
        if ($category === 255) {
            $sql = "SELECT * FROM wikimeta ORDER BY modtime DESC LIMIT :num offset :from";
        } else {
            $sql = "SELECT * FROM wikimeta WHERE category = :category ORDER BY modtime DESC LIMIT :num offset :from";
            $params[":category"] = $category;
        }
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);
        while($row = $stmt->fetch()) {
            $list[$i] = $row;
            $i++;
        }
        // if no record matches the query returns, failed
        if (!isset($list[0])) {
            return false;
        }
        return $list;
    }

    /**
     * Insert new post
     * @param int $id id of image
     * @param string $license Copyright, CC-BY-SA 3.0, X11, etc...
     * @param double $latitude latitude of the image
     * @param double $longitude longitude (those "double" is float in PHP)
     * @return int 0=success, -1=id not exists, -2=exception
     */
    public function insertPosts($id, $license, $latitude, $longitude) {
        try {
            if (!$this->isImagesIDExists($id)) {
                return -1;
            }
            $sql = "INSERT INTO posts (id, license, latitude, longitude) VALUES (:id, :license, :latitude, :longitude)";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':id' => $id, ':license' => $license, ':latitude' => $latitude, ':longitude' => $longitude);
            $stmt->execute($params);
        } catch (Exception $e) {
            echo $e->getMessage();
            return -2;
        }
        return 0;
    }

    /**
     * Select from posts
     * @param int $id id of image
     * @return array
     */
    public function selectPosts($id) {
        try {
            $sql = "SELECT id, license, latitude, longitude FROM posts WHERE id = :id";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':id' => $id);
            $stmt->execute($params);
            $res = $stmt->fetch();
            if ($res["id"] != $id) {
                return false;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
        return $res;
    }

    /**
     * check if id exists in images
     * @param int $id
     * @return bool
     */
    private function isImagesIDExists($id) {
        try {
            $sql = "SELECT id FROM images WHERE id = :id";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':id' => $id);
            $stmt->execute($params);
            $res = $stmt->fetch();
            if ($res["id"] != $id) {
                return false;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
        return true;
    }

    public function selectIdNumber() {
        try {
            $sql = "SELECT COUNT(id) FROM images";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_NUM);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $res[0];
    }

    public function selectUsers($id) {
        try {
            $sql = "SELECT * FROM users WHERE id = :id";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':id' => $id);
            $stmt->execute($params);
            $res = $stmt->fetch();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $res;
    }

    public function selectTags($tag) {
        try {
            $sql = "SELECT * FROM tags WHERE tag = :tag";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':tag' => $tag);
            $stmt->execute($params);
            $res = $stmt->fetch();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $res;
    }

    public function selectGalPage($from, $num) {
        try {
            $sql = "SELECT id, fname, ultime, modtime, title, author, tag, uuid, viewpass FROM images ORDER BY id DESC LIMIT :num offset :from";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':from' => $from, ':num' => $num);
            $stmt->execute($params);
            $i = 0;
            while($row = $stmt->fetch()) {
                $list[$i] = $row;
                $i++;
            }
    
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $list;
    }

    public function selectRssList($from, $num) {
        try {
            $sql = "SELECT id, ultime, title, author, comment, tag, viewpass FROM images ORDER BY id DESC LIMIT :num offset :from";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':from' => $from, ':num' => $num);
            $stmt->execute($params);
            $i = 0;
            while($row = $stmt->fetch()) {
                $list[$i] = $row;
                $i++;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $list;
    }

    public function selectIdfromTime($time) {
        try {
            $sql = "SELECT id, ultime FROM images WHERE ultime = :ultime";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':ultime' => $time);
            $stmt->execute($params);
            $res = $stmt->fetch();
            if ($res["ultime"] != $time) {
                return false;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $res["id"];
    }

    public static function getExt($filename) {
        $pos = strrpos($filename, ".") + 1;
        return substr($filename, $pos);
    }

    public function selectLastId() {
        try {
            $sql = "SELECT addr FROM users WHERE id = 0";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_NUM);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $res[0];
    }

    public function selectLastCommentId() {
        try {
            $sql = "SELECT MAX(id) FROM comments";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_NUM);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $res[0];
    }

    public function insertImages($id, $filename, $time, $title, $author, $authorid, $comment, $tag, $unique, $viewpass, $delpass, $md5) {
        try {
            $sql = "INSERT INTO images (id, fname, ultime, modtime, title, author, authorid, comment, tag, uuid, viewpass, delpass, md5) VALUES (:id, :fname, :ultime, :modtime, :title, :author, :authorid, :comment, :tag, :uuid, :viewpass, :delpass, :md5)";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':id' => $id, ':fname' => $filename, ':ultime' => $time, ':modtime' => $time, ':title' => $title, ':author' => $author, ':authorid' => $authorid, ':comment' => $comment, ':tag' => $tag, ':uuid' => $unique, ':viewpass' => $viewpass, ':delpass' => $delpass, ':md5' => $md5);
            $stmt->execute($params);
            //print_r($this->pdo->errorInfo());
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
        return true;
    }

    public function insertComments($toid, $comment, $author) {
        try {
            // warn: no toid check (yet...)
            if ($this->selectViewCommentsCount($toid) > 1000) {
                return false;
            }

            $lastid = $this->selectLastCommentId();
            $sql = "INSERT INTO comments (id, comment, author, upoint, modtime, toid) VALUES (:id, :comment, :author, :upoint, :modtime, :toid)";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':id' => $lastid + 1, ':comment' => $comment, ':author' => $author, ':upoint' => 0, ':modtime' => time(), ':toid' => $toid);
            $stmt->execute($params);
            //print_r($this->pdo->errorInfo());
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
        return true;
    }

    public function insertTags($tag, $pass, $info, $url, $until) {
        try {
            $sql = "INSERT INTO tags (tag, auth, info, until, url) VALUES (:tag, :auth, :info, :until, :url)";
            $stmt = $this->pdo->prepare($sql);
            $pass = password_hash($pass, PASSWORD_BCRYPT);
            $params = array(':tag' => $tag, ':auth' => $pass, ':info' => $info, ':url' => $url, ':until' => $until);
            $stmt->execute($params);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
        return true;
    }

    public function selectTaglist() {
        try {
            $sql = "SELECT tag, until, info, url FROM tags ORDER BY until DESC";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();
            $i = 0;
            while($row = $stmt->fetch()) {
                $list[$i] = $row;
                $i++;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $list;
    }

    public function deleteTags($tag) {
        try {
            if (is_null($tag) or $tag == "") {
                return false;
            }
            $sql = "DELETE FROM tags WHERE tag = :tag";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':tag' => $tag);
            $stmt->execute($params);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
        return true;
    }

    public function updateLastId($lastid) {
        try {
            $sql = "UPDATE users SET addr = :lastid WHERE id = 0";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':lastid' => $lastid);
            $stmt->execute($params);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
        return true;
    }

    public function selectView($id) {
        try {
            $sql = "SELECT id, fname, ultime, modtime, title, author, authorid, comment, tag, uuid, viewpass, md5 FROM images WHERE id = :id";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':id' => $id);
            $stmt->execute($params);
            $res = $stmt->fetch();
            if ($res["id"] != $id) {
                return false;
            }
            $res["authorid"] = substr(md5($res["authorid"]), -2);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $res;
    }

    public function selectViewComments($toid) {
        try {
            $sql = "SELECT id, comment, author, upoint, modtime, toid FROM comments WHERE toid = :toid ORDER BY id DESC";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':toid' => $toid);
            $stmt->execute($params);
            $i = 0;
            while($row = $stmt->fetch()) {
                $list[$i] = $row;
                $i++;
            }
            if (is_null($list[0])) {
                return false;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $list;
    }

    public function selectViewCommentsCount($toid) {
        try {
            $sql = "SELECT COUNT(id) FROM comments WHERE toid = :toid";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':toid' => $toid);
            $stmt->execute($params);
            $res = $stmt->fetch(PDO::FETCH_NUM);
            if (is_null($res)) {
                return false;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $res[0];
    }

    public function selectTagAll($tag) {
        try {
            $sql = "SELECT id, fname, title, author, modtime, comment, uuid, md5 FROM images WHERE tag = :tag";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':tag' => $tag);
            $stmt->execute($params);
            $i = 0;
            while($row = $stmt->fetch()) {
                $list[$i] = $row;
                $i++;
            }
            if ($i == 0) {
                return false;
            }
    
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $list;
    }

    public function deleteImages($id, $delpass) {
        try {
            $sql = "SELECT uuid, delpass FROM images WHERE id = :id";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':id' => $id);
            $stmt->execute($params);
            $res = $stmt->fetch();
            if (is_null($res)) {
                return false;
            }
            $resp = $this->selectUsers(0);
            if ($res["delpass"] == "" or password_verify($delpass, $res["delpass"])
                or password_verify($delpass, $resp["pass"])) {
                unlink("images/".$res["uuid"]);
                if (file_exists("images/".$res["uuid"]."_thumb")) {
                    unlink("images/".$res["uuid"]."_thumb");
                }
                if (file_exists("images/".$res["uuid"]."_b")) {
                    unlink("images/".$res["uuid"]."_b");
                }
            } else {
                return false;
            }
            $sql = "DELETE FROM images WHERE id = :id";
            $stmt = $this->pdo->prepare($sql);
            $params = array(':id' => $id);
            $stmt->execute($params);
            return true;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function close() {
        $this->pdo = NULL;
    }

    public function __destruct() {
        $this->close();
    }

}
