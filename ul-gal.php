<?php
/* Uler gallery uler/list/ 22/12/15 */

function galjson($from, $num) {
    global $dbfilename;

    header('Content-Type: application/json; charset=UTF-8');
    $uldb = new Uldb($dbfilename);
    $uldb->connect();
    if (is_numeric($from) === false or is_numeric($num) === false or $num > 25) {
        //http_response_code(400);
        if ($num == "last") {
            echo json_encode($uldb->selectIdNumber());
            exit();
        }
        echo json_encode("-2");
        exit();
    }
    $list = $uldb->selectGalPage($from, $num);
    for($i = 0; $i < $num; $i++) {
        if (isset($list[$i]["id"])) {
            if ($list[$i]["viewpass"] != "") {
                $list[$i]["passset"] = "true";
            } else {
                $list[$i]["passset"] = "false";
            }
        }
        unset($list[$i]["uuid"]);
        unset($list[$i]["viewpass"]);
    }
    if (is_null($list)) {
        //http_response_code(403);
        echo json_encode("-1");
        exit();
    }
    //$list["meta"]["last"] = $uldb->selectLastId();
    //$list["meta"]["count"] = $uldb->selectIdNumber();
    $uldb = NULL;
    echo json_encode($list);
    exit();
}

function galmain($page) {
    global $out;
    global $dbfilename;
    global $backto;

    if ($page == 0) {
        $page = 1;
    }
    $uldb = new Uldb($dbfilename);
    $uldb->connect();
    $last = $uldb->selectLastId() + 1;
    $idnumber = $uldb->selectIdNumber();
    $from = ($page-1) * 9;
    $list = $uldb->selectGalPage($from, 9);
    if (is_null($list)) {
        $out .= $from."Request failed";
        return -1;
    }

    $out .= "<table border=\"1\"><thead><tr><th colspan=\"3\">Page ".$page."</th></tr></thead><tbody>\r\n";

    $colcount = 0;
    $out .= "<tr>";
    for ($i = 0; $i < 9; $i++) {
        $out .= "<td width=\"200\" height=\"200\">";

        if (isset($list[$i])) {
            $out .= "<a href=\"../view/".$list[$i]["id"]."\">";
            if (file_exists("images/".$list[$i]["uuid"]."_thumb") and $list[$i]["viewpass"] == "") {
                $out .= "<img src=\"../thumb/".$list[$i]["id"].".jpg\"></a><br>".$list[$i]["title"];
            } else {
                $out .= $list[$i]["title"]."</a>";
            }
        }
        
        $out .= "</td>";
        $colcount++;
        if ($colcount > 2) {
            $out .= "</tr><tr>";
            $colcount = 0;
        }
    }

    $out .= "</tr></tbody></table>";

    for ($i = 0; $i < $idnumber / 9; $i++) {
        $inum = $i + 1;
        $out .= "<a href=\"".$inum."\">".$inum."</a>";
    }
    $uldb = NULL;
    $backto = "../../";
    return 1;
}