<?php
/* Uler view uler/view/ 22/11/03 */

function viewimage($id, $thumb, $ext) {
    global $dbfilename;

    $uldb = new Uldb($dbfilename);
    $uldb->connect();
    $res = $uldb->selectView($id);
    $uldb = NULL;
    if ($res === false) {
        return false;
    }
    if ($res["viewpass"] != "") {
        $pass = $_POST["viewpass"];
        $authres = viewverifypass($pass, $res["viewpass"]);
        if ($authres == -1) {
            return false;
        } elseif ($authres == -2) {
            return false;
        }
    }
    if ($thumb == 1) {
        $srcname = "images/".$res["uuid"]."_thumb";
    } else {
        $srcname = "images/".$res["uuid"];
    }
    $filetype = exif_imagetype($srcname);
    header("Content-Disposition: inline; filename=\"".$res["fname"]."\"");
    if ($filetype == 1) {
        header('Content-Type: image/gif');
    } elseif ($filetype == 2) {
        header('Content-Type: image/jpeg');
    } elseif ($filetype == 3) {
        header('Content-Type: image/png');
    } elseif ($filetype == 6) {
        header('Content-Type: image/bmp');
    } elseif ($filetype == 15) {
        header('Content-Type: image/vnd.wap.wbmp');
    } elseif ($filetype == 16) {
        header('Content-Type: image/x-xbitmap');
    } else {
        if ($thumb == 1) {
            return false;
        }
        $ext = strtolower(Uldb::getExt($res["fname"]));
        if ($ext == "zip") {
            header('Content-Type: application/x-zip-compressed');
        } elseif ($ext == "lzh" or $ext == "lha") {
            header('Content-Type: application/x-lzh');
        } elseif ($ext == "doc") {
            header('Content-Type: application/msword');
        } elseif ($ext == "docx") {
            header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        } elseif ($ext == "mp3") {
            header('Content-Type: audio/mpeg');
        } elseif ($ext == "mp4") {
            header('Content-Type: video/mp4');
        } elseif ($ext == "pdf") {
            header('Content-Type: application/pdf');
        } elseif ($ext == "mid") {
            header('Content-Type: audio/midi');
        }  else {
            header('Content-Type: application/octet-stream');
        }
    }
    if ($ext === "b64" and $filetype > 0 and $filetype < 17) {
        $outimage = file_get_contents($srcname);
        echo base64_encode($outimage);
    } else {
        header('Content-Length: '.filesize($srcname));
        readfile($srcname);
    }
    return true;
}

function viewdelete($id, $delpass) {
    global $dbfilename;

    if (isset($id) === false or $id == "") {
        return false;
    }
    if (strlen($delpass) > 20) {
        return false;
    }
    $uldb = new Uldb($dbfilename);
    $uldb->connect();
    $res = $uldb->deleteImages($id, $delpass);
    $uldb = NULL;
    return $res;
}

function viewverifypass($post, $db) {
    if ($post == "") {
        return -2;
    } elseif (password_verify($post, $db) === false) {
        return -1;
    }
}

function viewuserauth($tag, $pass) {
    global $dbfilename;

    $uldb = new Uldb($dbfilename);
    $uldb->connect();
    $res = $uldb->selectTags($tag);
    $uldb = NULL;
    if (is_null($res) === false and $res["auth"] != "" and password_verify($pass, $res["auth"])) {
        return true;
    } else {
        return false;
    }
}

function viewzipall($tag, $pass) {
    global $dbfilename;
    global $zippath;

    if (is_null($tag) or $tag == "") {
        echo "<form action=\"./zip\" method=\"POST\">tag: <input type=\"text\" name=\"ziptag\"><br>password: <input type=\"text\" name=\"pass\"><input type=\"submit\"></form>";
        echo "<a href=\"../list\">back</a>";
        return 2;
    }

    if (viewuserauth($tag, $pass) === false) {
        return -3;
    }

    $uldb = new Uldb($dbfilename);
    $uldb->connect();
    $res = $uldb->selectTagAll($tag);
    if ($res === false) {
        return -2;
    }
    $out = "";
    $csvout = "id,file,title,author,comment,filename,md5,modtime\r\n";

    if (file_exists($zippath.$tag.".zip")) {
        unlink($zippath.$tag.".zip");
    }
    $zip = new ZipArchive();
    if ($zip->open($zippath.$tag.".zip", ZipArchive::CREATE) === false) {
        return -1;
    }
    // build comments and authors list
    for ($i = 0; $i < count($res); $i++) {
        $id = $res[$i]["id"];
        $ext = Uldb::getExt($res[$i]["fname"]);
        $fname = $res[$i]["fname"];
        $title = $res[$i]["title"];
        $author = $res[$i]["author"];
        $comment = $res[$i]["comment"];
        $md5 = $res[$i]["md5"];
        $modtime = date("Y/m/d H:i:s", $res[$i]["modtime"]);
        $out .= $id."\r\n\r\nTitle:\r\n\r\n".$title."\r\n\r\nAuthor:\r\n\r\n".$author."\r\n\r\n";
        $out .= "Comment:\r\n\r\n".$comment."\r\n\r\n\r\n\r\n";
        $title = str_replace("\"", "\"\"", htmlspecialchars_decode($title));
        $author = str_replace("\"", "\"\"", htmlspecialchars_decode($author));
        $comment = str_replace("\"", "\"\"", htmlspecialchars_decode($comment));
        $csvout .= $id.",".$id.".".$ext.",\"".$title."\",\"".$author."\",\"".$comment."\",\"".$fname."\",".$md5.",".$modtime."\r\n";

    }
    $zip->addFromString("_details.txt", $out);
    $bom = pack('C*',0xEF,0xBB,0xBF);
    $zip->addFromString("_details.csv", $bom.$csvout);
    // add files to zip archive
    for ($i = 0; $i < count($res); $i++) {
        $src = "images/".$res[$i]["uuid"];
        $ext = Uldb::getExt($res[$i]["fname"]);
        $id = $res[$i]["id"];
        $zip->addFile($src, $id.".".$ext);
    }
    $zip->close();
    $zip = NULL;
    return 1;
}

function viewzipdl($tag, $pass) {
    global $zippath;

    if (!file_exists($zippath.$tag.".zip")) {
        return -1;
    }
    if (!viewuserauth($tag, $pass)) {
        return -3;
    }

    header("Content-Disposition: inline; filename=\"".$tag.".zip\"");
    header("Content-Type: application/x-zip-compressed");
    readfile($zippath.$tag.".zip");
    return 1;
}

function viewpostcomment($toid, $comment, $author) {
    global $dbfilename;

    if ($toid == "" or $comment == "") {
        return -1;
    }
    if (is_numeric($toid) === false) {
        return -1;
    }
    if (mb_strlen($comment) > 250 or mb_strlen($author) > 50) {
        return -1;
    }
    if ($author)
    //$comment = str_replace("\r\n", "", $comment);
    $author = str_replace("\r\n", "", $author);
    if ($author == "" or isset($author) === false) {
        $author = "";
    }
    $uldb = new Uldb($dbfilename);
    $uldb->connect();
    if ($uldb->selectView($toid) === false) {
        return -2;
    }
    $uldb->insertComments($toid, $comment, $author);
    return 1;
}

function viewdetail($id, $json) {
    global $dbfilename;

    if (is_numeric($id) === false) {
        return false;
    }
    $uldb = new Uldb($dbfilename);
    $uldb->connect();
    $res = $uldb->selectView($id);
    $comments = $uldb->selectViewComments($id);
    $uldb = NULL;
    if ($res === false) {
        if (isset($json)) {
            header('Content-Type: application/json; charset=UTF-8');
            echo json_encode(array('code' => -1));
            exit();
        }
        return false;
    }
    if ($res["viewpass"] != "") {
        $pass = $_POST["viewpass"];
        $authres = viewverifypass($pass, $res["viewpass"]);
        if ($authres == -1) {
            if (isset($json)) {
                header('Content-Type: application/json; charset=UTF-8');
                echo json_encode(array('code' => -2));
                exit();
            }
            return false;
        } elseif ($authres == -2) {
            if (isset($json)) {
                header('Content-Type: application/json; charset=UTF-8');
                echo json_encode(array('code' => -3));
                exit();
            }
                echo "password required";
                echo "<form action=\"".$id."\" method=\"POST\"><input type=\"text\" name=\"viewpass\"><input type=\"submit\"></form>";
            return true;
        }
    }
    $res["filesize"] = filesize("images/".$res["uuid"]);
    if (isset($json)) {
        header('Content-Type: application/json; charset=UTF-8');
        unset($res["uuid"]);
        unset($res["viewpass"]);
        $res["comments"] = $comments;
        $res["title"] = htmlspecialchars_decode($res["title"]);
        $res["author"] = htmlspecialchars_decode($res["author"]);
        $res["code"] = 0;
        echo json_encode($res);
        exit();
    }
    if ($res["title"] == "") {
        $res["title"] = "empty";
    }
    if ($res["comment"] == "") {
        $res["comment"] = "empty";
    } else {
        $res["comment"] = nl2br($res["comment"]);
    }
    if ($res["tag"] == "") {
        $res["tag"] = "empty";
    }
    $fileext = strtolower(Uldb::getExt($res["fname"]));
    $out = "<html><head><title>Uler</title></head><body>";
    if (file_exists("images/".$res["uuid"]."_thumb") and $res["viewpass"] == "") {
    $out .= "<img src=\"".$id.".".$fileext."\">\r\n";
    }
    if ($res["viewpass"] != "") {
        $out .= "<hr><form action=\"".$id.".".$fileext."\" method=\"POST\"><input type=\"hidden\" name=\"viewpass\" value=\"".$pass."\"><input type=\"submit\" value=\"Download\"></form></a><br>\r\n";
    } else {
        $out .= "<hr><a href=\"".$id.".".$fileext."\">Download</a><br>\r\n";
    }
    $out .= "Title: ".$res["title"]."<br>Author: ".$res["author"]." (id:".$res["authorid"].")<br>Tag: ".$res["tag"]."<br>Comment:<br>\r\n".$res["comment"]."<br>Filename: ".$res["fname"]."<br>MD5: ".$res["md5"]."<br>Upload date: ".date(DATE_RFC2822, $res["ultime"])."<br>\r\n";
    $out .= "<form action=\"./".$id."\" method=POST>Delete pass:<input type=\"text\" name=\"delpass\"><input type=\"submit\"></form><br>\r\n";

    $out .= "<hr>User comments:<br>";
    $out .= "<form action=\"".$id."\" method=\"POST\"><input type=\"text\" name=\"comment\">Name: <input type=\"text\" name=\"author\"><input type=\"submit\"></form>";
    if ($comments !== false) {
        for ($i = 0; $i < count($comments); $i++) {
            if ($comments[$i]["author"] == "") {
                $comments[$i]["author"] = "Anonymous";
            }
            $out .= $comments[$i]["id"].". ".htmlspecialchars($comments[$i]["comment"])." --- ".htmlspecialchars($comments[$i]["author"])." (".date(DATE_RFC2822, $comments[$i]["modtime"]).")<br>\r\n";
        }
    } else {
        $out .= "No comments available.<br>\r\n";
    }

    $out .= "<br><a href=\"../list/\">back</a>";
    echo $out;
    return true;
}

function viewmain() {
    if (isset($_GET["ext"])) {
        if ($_GET["thumb"] == "1") {
            $res = viewimage($_GET["id"], 1, $_GET["ext"]);
        } else {
            $res = viewimage($_GET["id"], 0, $_GET["ext"]);
        }
    } else {
        if (isset($_POST["delpass"])) {
            $res = viewdelete($_GET["id"], $_POST["delpass"]);
            if ($res === true) {
                if ($_POST["json"]) {
                    echo json_encode($res);
                } else {
                    echo "File deleted.";
                }
            }
        } elseif (isset($_POST["comment"]) and isset($_POST["author"])) {
            $res = viewpostcomment($_GET["id"], $_POST["comment"], $_POST["author"]);
            if ($res > 0) {
                if ($_POST["json"]) {
                    echo json_encode($res);
                } else {
                    echo "Comment posted.";
                }
            }
        } elseif ($_GET["zip"] == "1") {
            $res = viewzipall($_POST["ziptag"], $_POST["pass"]);
            if ($res == 1) {
                if ($_POST["json"]) {
                    echo json_encode($res);
                } else {
                    echo "Succeed, <a href=\"../zip/".$_POST["ziptag"].".zip\">".$_POST["ziptag"].".zip</a><br>";
                    echo "<a href=\"../list/\">back</a>";
                }
            }
        } elseif ($_GET["zipdl"] == "1") {
            $res = viewzipdl($_POST["ziptag"], $_POST["pass"]);
            if ($res == 1) {
                exit();
            }
        } else {
            $res = viewdetail($_GET["id"], $_GET["json"]);
        }
    }
    if ($res === false or $res < 0) {
        if ($_POST["json"]) {
            echo json_encode($res);
        } else {
            echo "Request failed";
        }
    }
}