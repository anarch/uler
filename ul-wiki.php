<?php
/**
 * @file ul-wiki.php
 * @brief wiki
 * @date 2024/1/12
 */

class UlWikiInitException extends Exception { }

/**
 * @date 2024/1/12
 */
interface UlWikiHTMLGenerator {
    /**
     * Sanitize string
     * @param string $content
     * @param int $mode 0=html, 1=url
     * @return string
     */
    public function SanitizeString($content, $mode);
    /**
     * Set basic settings, please do this first
     * @param string $name name of the page
     * @return int 0=success, 1=failure
     */
    public function SetBasicSettings($name);
    /**
     * Set header (not http header nor html head)
     * @param string $version version of this software
     * @param int $mode 0=normal, 1=off, 2=edit, 3=search, 4=top
     * @return int 0=success, 1=failure
     */
    public function SetHeader($version, $mode);
    /**
     * Set content
     * @param string $content content
     * @return int 0=success, 1=failure
     */
    public function SetContent($content);
    /**
     * Set footer
     * @param string $back path to back
     * @param int $mode 0=normal, 1=off, 2=top
     * @return int 0=success, 1=failure
     */
    public function SetFooter($back, $mode);
    /**
     * Generate HTML
     * @return string Generated HTML
     */
    public function Generate();
}

/**
 * Implement of basic HTML Generator
 * @date 2024/1/12
 */
class UlWikiBasicHTMLGenerator implements UlWikiHTMLGenerator {
    private $out = "";
    private $htmlTitle = "";
    private $htmlHeader = "";
    private $htmlContent = "";
    private $htmlFooter = "";

    private $htmlLastUpdate = 0;

    public function SanitizeString($content, $mode) {
        if ($mode) {
            return urlencode($content);
        } else {
            return htmlspecialchars($content, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401, "UTF-8");
        }
    }

    public function SetBasicSettings($name) {
        // set title tag
        $this->htmlTitle = $name;
        return 0;
    }
    /**
     * set Lastupdate time
     * @param int $time time in unixtime
     */
    public function SetLastUpdate($time) {
        $this->htmlLastUpdate = $time;
    }
    public function SetHeader($version, $mode) {
        // if mode is 0, skip this sector
        if ($mode === 1)
            return 0;
        // insert version
        $this->htmlHeader = '<font color="blue">'.$version."</font>";
        // insert page name
        $this->htmlHeader .= ' '.$this->htmlTitle;
        // insert menus
        // if not editor or search, insert edit menu
        if ($mode !== 2 and $mode !== 3) {
            if ($mode !== 4) {
                $this->htmlHeader .= ' <a href="../?e&name='.$this->htmlTitle.'">Edit</a> <a href="../?m&name='.$this->htmlTitle.'">MetaEdit</a>';
            } else {
                $this->htmlHeader .= ' <a href="./?e&name=IndexPage">Edit</a>';
            }
        }
        // if not search, insert search menu
        if ($mode !== 3)
            if ($mode !== 4) {
                $this->htmlHeader .= ' <a href="../?s">Search</a>'."\r\n";
            } else {
                $this->htmlHeader .= ' <a href="./?s">Search</a>'."\r\n";
            }
        // if top, insert newpage
        if ($mode === 4)
            $this->htmlHeader .= '<a href="uler/list/">Upload</a> <a href="./?n">NewPage</a>'."\r\n";
        $this->htmlHeader .= "<br><hr>\r\n";
        return 0;
    }
    public function SetContent($content) {
        $this->htmlContent = $content;
        return 0;
    }
    public function SetFooter($back, $mode) {
        // if mode is off, skip this sector 
        if ($mode === 1)
            return 0;
        $this->htmlFooter = "<hr>";
        if ($mode !== 2)
            $this->htmlFooter .= '<a href="'.$back.'">Back</a>';
        if ($this->htmlLastUpdate and $mode !== 2)
            $this->htmlFooter .= " ";
        if ($this->htmlLastUpdate)
            $this->htmlFooter .= "<i>Last Update: ".date(DATE_RFC2822, $this->htmlLastUpdate)."</i>";
        return 0;
    }
    public function Generate() {
        $this->out = "<!DOCTYPE html>\r\n";
        $this->out .= "<html><head>\r\n";
        $this->out .= "<title>".$this->htmlTitle."</title>\r\n";
        $this->out .= "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n";
        $this->out .= "</head><body>\r\n";
        $this->out .= $this->htmlHeader;
        $this->out .= $this->htmlContent;
        $this->out .= $this->htmlFooter;
        $this->out .= "</body></html>";
        return $this->out;
    }
}

/**
 * set header, it takes effect immediately and not reversible
 * @date 2024/1/21
 */
class UlWikiSetHeader {
    /**
     * set "Last-Modified" header in Header
     * @param int $mtime unix time
     * @return bool
     */
    public function SetLastModified($mtime) {
        header('Last-Modified: '.gmdate('D, d M Y H:i:s', $mtime)." GMT");
        return true;
    }
}

/**
 * wiki
 * @date 2024/1/12
 */
class UlWiki {
    private $ulDB;
    private $ulWikiHTML;
    private $ulWikiSetHeader;

    private $ulArticleName;
    private $ulArticleAuthor;
    private $ulArticleContent;
    private $ulArticleCreatedTime;
    private $ulMetaContent;
    private $ulMetaCategory;
    private $ulMetaLink;
    private $ulMetaManufacturer;
    private $ulMetaRate;
    private $ulMetaLatitude;
    private $ulMetaLongitude;

    private $ulPagerCurrent;
    private $ulPagerLength;
    private $ulPagerBase;

    private $ulWikiVersion = "MilkyWiki";
    private $ulCategory = array("none", "item", "food", "book", "music", "software", "place");

    /* please edit here to your uler setup! 
    like: http://example.com/ and http://example.com/uler/    
    */
    private $ulWikiPath;
    private $ulUlerPath;
    /* edit end */

    private $ulWikiOutputBuffer;

    /**
     * @param string $name filename of database
     */
    public function __construct($name) {
        if (!empty($name) and is_string($name)) {
            $this->ulDB = new Uldb($name);
            $this->ulDB->connect();
        } else {
            throw new UlWikiInitException('Database name not valid');
        }
        $this->ulWikiHTML = new UlWikiBasicHTMLGenerator();
        $this->ulWikiSetHeader = new UlWikiSetHeader();
        if (isset($_SERVER['HTTPS'])) {
            $this->ulWikiPath = "https://wiki.klovnin.net/";
            $this->ulUlerPath = "https://wiki.klovnin.net/uler/";
        } else {
            $this->ulWikiPath = "http://wiki.klovnin.net/";
            $this->ulUlerPath = "http://wiki.klovnin.net/uler/";
        }
    }

    /**
     * check if article name is valid
     * @param string $name article name
     * @return int true=valid
     */
    private function IsValidArticleName($name) {
        if (!$name)
            return false;
        if (mb_strlen($name) > 127)
            return false;
        if (strpos($name, '&') or strpos($name, '#') or strpos($name, '?') or strpos($name, '/')
            or $name == "InvalidRequest")
            return false;
        return true;
    }

    /**
     * check if metadata is valid
     * @return int
     */
    private function IsValidMetadata($category, $link, $manufacturer, $rate, $contentshort, $latitude, $longitude) {
        if (!isset($category) or $category > 254 or !is_numeric($category))
            return -1;
        if (strlen($link) > 2047)
            return -2;
        if (strlen($manufacturer) > 1023)
            return -3;
        if (!isset($rate) or !is_numeric($rate) or $rate < 0 or $rate > 10)
            return -4;
        if (strlen($contentshort) > 2047)
            return -5;
        if ($latitude != "" or $longitude != "") {
            if ($latitude == "" or $longitude == "" or strlen($latitude) > 32 or strlen($longitude) > 32 or !is_numeric($latitude) or !is_numeric($longitude))
                return -6;
            if ($latitude > 90 or $latitude < -90)
                return -7;
            if ($longitude > 180 or $longitude <= -180)
                return -8;
        }
        return 0;
    }

    /**
     * Convert string to crc32 hex string
     * @param string $content
     * @return string crc32 hex in string
     */
    private function ConvertStringToCRC32Hex($content) {
        return dechex(crc32($content));
    }

    /**
     * Set article name, HTML sanitized
     * @param string $name
     * @return bool
     */
    private function SetArticleName($name) {
        $this->ulArticleName = $this->ulWikiHTML->SanitizeString($name, 0);
        return;
    }

    /**
     * Loading article from db to ulArticle
     * @param int $id article id
     * @return int 0=success -1=id not found -2=content not found
     */
    private function LoadArticle($id) {
        if (!$ctime = $this->ulDB->selectWikiArticleLatest($id)) {
            return -1;
        }
        if (!$acont = $this->ulDB->selectWikiArticleContent($id, $ctime)) {
            return -2;
        }
        $this->ulArticleContent = $acont["content"];
        $this->ulArticleAuthor = $acont["author"];
        $this->ulArticleCreatedTime = $acont["ctime"];
        return 0;
    }

    private function LoadArticleMeta($id) {
        if ($this->ulDB->selectWikiArticleMeta($id) < 0) {
            return -1;
        }
        $metares = $this->ulDB->selectWikiArticleMeta($id);
        $this->ulMetaContent = $metares["contentshort"];
        $this->ulMetaCategory = $metares["category"];
        $this->ulMetaLink = $metares["link"];
        $this->ulMetaManufacturer = $metares["manufacturer"];
        $this->ulMetaRate = $metares["rate"];
        $this->ulMetaLatitude = $metares["latitude"];
        $this->ulMetaLongitude = $metares["longitude"];
        return 0;
    }

    /**
     * Load article for not found screen
     * @param string $name article name
     * @return int 
     */
    private function LoadArticleNotfound($name) {
        $this->ulArticleContent = "Not found";
        $this->SetArticleName($name);
        return 0;
    }

    /**
     * Load new article for edit screen
     * @param string $name article name
     * @return int
     */
    private function LoadArticleEditNotfound($name) {
        $this->SetArticleName($name);
        return 0;
    }

    /**
     * Load article for invalid page
     * @return int
     */
    private function LoadArticleInvalid() {
        $this->ulArticleContent = "Invalid Request";
        $this->SetArticleName("InvalidRequest");
        return 0;
    }

    /**
     * Convert ulArticleContent to HTML
     * @return string if failed, false
     */
    private function ConvertArticletoHTML() {
        if ($this->ulArticleContent === NULL) {
            return false;
        }
        $aarr = explode("\r\n", $this->ulWikiHTML->SanitizeString($this->ulArticleContent, 0));
        for ($i = 0; $i < count($aarr); $i++) {
            switch (substr($aarr[$i], 0, 2)) {
                case ":a":
                    $url = substr($aarr[$i], 2);
                    if (substr($url, 0, 5) == "http:" or substr($url, 0, 6) == "https:" or
                        substr($url, 0, 4) == "ftp:" or substr($url, 0, 5) == "ftps:" or
                        substr($url, 0, 7) == "gopher:" or substr($url, 0, 7) == "mailto:" or
                        substr($url, 0, 5) == "news:") {
                        $aarr[$i] = '<a href="'.$url.'">'.$url.'</a><br>';
                    } else {
                        if ($fragpos = strrpos($url, '&')) {
                            $fragment = substr($url, $fragpos + 5);
                            $fragmentid = '#'.$this->ConvertStringToCRC32Hex($fragment);
                            $fragment = '#'.$fragment;
                            $url = substr($url, 0, $fragpos);
                        } else {
                            $fragment = "";
                            $fragmentid = "";
                        }
                        $aarr[$i] = '<a href="'.$this->ulWikiPath.$url.'/'.$fragmentid.'">'.$url.$fragment.'</a><br>';
                    }
                    break;
                case ":b":
                    $strong = substr($aarr[$i], 2);
                    $aarr[$i] = '<strong id="'.$this->ConvertStringToCRC32Hex($strong).'">'.$strong.'</strong><br>';
                    break;
                case ":r":
                    $red = substr($aarr[$i], 2);
                    $aarr[$i] = '<font color="red">'.$red.'</font><br>';
                    break;
                case ":h":
                    $aarr[$i] = '<hr>';
                    $nobr = true;
                    break;
                case ":i":
                    $image = substr($aarr[$i], 2);
                    $aarr[$i] = '<a href="'.$this->ulUlerPath.'view/'.$image.'"><img src="'.$this->ulUlerPath.'thumb/'.$image.'.jpg"></a><br>';
                    break;
                default:
                    $aarr[$i] = $aarr[$i].'<br>';
                    break;
            }
        }
        $html = implode("\r\n", $aarr);
        return $html;
    }

    /**
     * Convert meta variables to HTML
     * @return string
     */
    private function ConvertMetatoHTML($category, $link, $manufacturer, $rate, $contentshort, $latitude, $longitude) {
        $html = "<hr>Metadata:<br>";
        $html .= $category.",".$link.",".$manufacturer.",".$rate.",".$contentshort.",".$latitude.",".$longitude;
        return $html;
    }

    /**
     * Create new article
     * @param string $name name of article
     * @return int article id, -1=failed
     */
    private function CreateArticle($name) {
        $id = $this->ulDB->selectWikiLastID() + 1;
        if ($this->ulDB->insertWikiArticle($id, $name)) {
            return $id;
        } else {
            return -1;
        }
    }

    /**
     * Insert new article content
     * @param int $id article id
     * @param string $author author name
     * @param string $content content
     * @return bool
     */
    private function InsertArticleContent($id, $author, $content) {
        if ($content == "del") {
            return $this->ulDB->deleteWikiArticle($id);
        } else { 
            if ($this->ulDB->insertWikiArticleContent($id, $author, $content)) {
                return 0;
            } else {
                return -1;
            }
        }
    }

    /**
     * Delete Article
     * @param int $id article id
     * @return int
     */
    private function DeleteArticle($id) {
        return $this->ulDB->deleteWikiArticle($id);
    }

    /**
     * Update or Insert Wiki meta
     * @param int $id article id
     * @param int $category category of article
     * @param string $link image ID or URL
     * @param string $manufacturer
     * @param int $rate rate of that item, 1-5
     * @param string $contentshort short description of article
     * @param float $latitude latitude
     * @param float $longitude longitude
     * @return int
     */
    private function UpdateArticleMeta($id, $category, $link, $manufacturer, $rate, $contentshort, $latitude, $longitude) {
        if ($this->ulDB->selectWikiArticleMeta($id) < 0) {
            $this->ulDB->insertWikiArticleMeta($id, $category, time(), $link, $latitude, $longitude, $manufacturer, $rate, $contentshort);
        } else {
            $this->ulDB->updateWikiArticleMeta($id, $category, time(), $link, $latitude, $longitude, $manufacturer, $rate, $contentshort);
        }
    }

    /**
     * generate list html from array
     * @param array $list array of id and aname
     * @return string|bool
     */
    private function GenerateArrayHTML($list) {
        if (!is_array($list) or count($list) === 0)
            return false;
        $html = "";
        for ($i = 0; $i < count($list); $i++) {
            $html .= '<a href="'.$this->ulWikiHTML->SanitizeString($list[$i]["aname"], 0).'/">'.$i.' '.$this->ulWikiHTML->SanitizeString($list[$i]["aname"], 0)."</a><br>\r\n";
        }
        return $html;
    }

    /**
     * generate pager
     * @param int $cur current page
     * @param int $total total number of page
     * @param string $url base url of links
     * @return string|bool
     */
    private function GeneratePagerHTML($cur, $total, $url) {
        if ($cur > $total)
            return -1;
        if ($cur < 1)
            return -2;
        $html = "Page ".$cur."<br>\r\n";
        $j = 0;
        for ($i = 1; $i < $total+1; $i++) {
            if ($i == $cur) {
                $html .= $i.' ';
            } else {
                $html .= '<a href="'.$url.$i.'">'.$i."</a>".' ';
            }
            if ($j > 9) {
                $html .= "<br>\r\n";
                $j = 0;
            } else {
                $j++;
            }
        }
        return $html;
    }

    /**
     * convert category id to name
     * @param int $categoryid
     * @return string
     */
    private function ConvertCategoryIDtoName($categoryid) {
        if ($categoryid <= count($this->ulCategory)) {
            return $this->ulCategory[$categoryid];
        } elseif ($categoryid < 128) {
            return $categoryid;
        } elseif ($categoryid == 255) {
            return "all";
        } else {
            $tempcategory = $categoryid - 127;
            return "user".$tempcategory;
        }
    }


    /**
     * generate table html from meta array
     * @param array $list array of metadata
     * @return string|bool
     */
    private function GenerateMetaArrayHTML($list) {
        if (!is_array($list) or count($list) === 0)
            return false;
        $html = '<table border="1"><thead><tr><th>number</th><th>category</th><th>name</th><th>link</th><th>manufacturer</th><th>rate</th><th>contentshort</th><th>latitude</th><th>longitude</th></tr></thead><tbody>';
        for ($i = 0; $i < count($list); $i++) {
            $html .= '<tr><td>'.$i.'</td>';
            $html .= '<td>'.$this->ConvertCategoryIDtoName($list[$i]["category"]).'</td>';
            $html .= '<td><a href="'.$list[$i]["aname"].'/">'.$this->ulWikiHTML->SanitizeString($list[$i]["aname"], 0).'</a></td>';
            if (isset($list[$i]["link"]) and $list[$i]["link"] != "") {
                $html .= '<td><a href="'.$this->ulWikiHTML->SanitizeString($list[$i]["link"], 0).'">available</a></td>';
            } else {
                $html .= '<td></td>';
            }
            $html .= '<td>'.$this->ulWikiHTML->SanitizeString($list[$i]["manufacturer"], 0)."</td>";
            if ($list[$i]["rate"] == 0) {
                $html .= "<td></td>";
            } else {
                $html .= "<td>".$list[$i]["rate"]."</td>";
            }
            $html .= "<td>".$this->ulWikiHTML->SanitizeString($list[$i]["contentshort"], 0)."</td>";
            $html .= "<td>".$this->ulWikiHTML->SanitizeString($list[$i]["latitude"], 0)."</td>";
            $html .= "<td>".$this->ulWikiHTML->SanitizeString($list[$i]["longitude"], 0)."</td></tr>\r\n";
        }
        $html .= "</tbody></table>";
        return $html;
    }
    /**
     * insert article name to meta array
     * @param array $list array of metadata
     * @return array
     */
    private function InsertMetaArrayArticleName($list) {
        $ret = $list;
        for ($i = 0; $i < count($list); $i++) {
            $ret[$i]["aname"] = $this->ulDB->selectWikiArticleNamefromID($ret[$i]["id"]);
        }
        return $ret;
    }

    /**
     * Article HTML render and write it to ulWikiOutputBuffer
     * @param int $name article name
     * @return int 0=success -1=failed
     */
    public function ArticleHTMLRender($name) {
        $isexistmeta = false;
        $htmlmeta = "";
        if (!$this->IsValidArticleName($name)) {
            $this->LoadArticleInvalid();
        } elseif ($id = $this->ulDB->selectWikiArticleIDfromName($name)) {
            if ($this->LoadArticle($id) < 0) {
                $this->LoadArticleNotfound($name);
            }
            $this->SetArticleName($name);
        } else {
            $this->LoadArticleNotfound($name);
        }
        if (isset($id) and $this->LoadArticleMeta($id) > -1)
            $isexistmeta = true;
        $this->ulWikiHTML->SetBasicSettings($this->ulArticleName);
        $this->ulWikiHTML->SetHeader($this->ulWikiVersion, 0);
        $html = $this->ConvertArticletoHTML($this->ulArticleContent);
        if ($isexistmeta)
            $htmlmeta = $this->ConvertMetatoHTML(
                $this->ulMetaCategory,
                $this->ulMetaLink,
                $this->ulMetaManufacturer,
                $this->ulMetaRate,
                $this->ulMetaContent,
                $this->ulMetaLatitude,
                $this->ulMetaLongitude);
        $this->ulWikiHTML->SetContent($html.$htmlmeta);
        $this->ulWikiSetHeader->SetLastModified($this->ulArticleCreatedTime);
        $this->ulWikiHTML->SetLastUpdate($this->ulArticleCreatedTime);
        $this->ulWikiHTML->SetFooter("../", 0);
        $this->ulWikiOutputBuffer = $this->ulWikiHTML->Generate();
        return 0;
    }

    /**
     * Article top HTML render and write it to ulWikiOutputBuffer
     * @return int 0=success -1=failed
     */
    public function ArticleTopHTMLRender() {
        if ($id = $this->ulDB->selectWikiArticleIDfromName("IndexPage")) {
            $this->LoadArticle($id);
            $this->SetArticleName("IndexPage");
        } else {
            $this->LoadArticleNotfound("IndexPage");
        }
        $this->ulWikiHTML->SetBasicSettings($this->ulArticleName);
        $this->ulWikiHTML->SetHeader($this->ulWikiVersion, 4);
        $html = $this->ConvertArticletoHTML($this->ulArticleContent);
        $this->ulWikiHTML->SetContent($html);
        $this->ulWikiSetHeader->SetLastModified($this->ulArticleCreatedTime);
        $this->ulWikiHTML->SetLastUpdate($this->ulArticleCreatedTime);
        $this->ulWikiHTML->SetFooter("../", 2);
        $this->ulWikiOutputBuffer = $this->ulWikiHTML->Generate();
        return 0;
    }

    /**
     * Editor HTML render and write it to ulWikiOutputBuffer
     * @param string $name article name
     * @return int 0=success -1=failed
     */
    public function EditorHTMLRender($name) {
        if (!$this->IsValidArticleName($name)) {
            $this->LoadArticleInvalid();
        } elseif ($id = $this->ulDB->selectWikiArticleIDfromName($name)) {
            $this->LoadArticle($id);
            $this->SetArticleName($name);
        } else {
            $this->LoadArticleEditNotfound($name);
        }
        $this->ulWikiHTML->SetBasicSettings($this->ulArticleName." - Edit");
        $this->ulWikiHTML->SetHeader($this->ulWikiVersion, 2);
        $html = ":a[article&frag] or :a[url] to link, :b to bold, :r to red<br>\r\n";
        $html .= ":i[id] to display image, :h to horizontal line, leave blank to delete<br>\r\n";
        $html .= '<form action="?e&name='.$name.'" method="POST"><textarea name="content" rows="24" cols="80">'.$this->ulWikiHTML->SanitizeString($this->ulArticleContent, 0).'</textarea><br>'."\r\n";
        $html .= '<input type="submit"></form>';
        $this->ulWikiHTML->SetContent($html);
        $this->ulWikiHTML->SetFooter("./", 0);
        $this->ulWikiOutputBuffer = $this->ulWikiHTML->Generate();
        return 0;
    }

    /**
     * Editor result HTML render and write it to ulWikiOutputBuffer
     * @param string $name article name
     * @param string $author author of article
     * @param string $content content of article
     * @return int 0=success, -1=create failed, -2=update failed
     */
    public function EditorResultHTMLRender($name, $author, $content) {
        $ret = -1;
        if (!$this->IsValidArticleName($name)) {
            $this->ulWikiHTML->SetBasicSettings("InvalidRequest");
            $this->LoadArticleInvalid();
            $this->ulWikiHTML->SetContent($this->ulArticleContent);
        } else {
            if (!$id = $this->ulDB->selectWikiArticleIDfromName($name)) {
                if ($content != "") {
                    if (!$id = $this->CreateArticle($name)) {
                        $ret = -1;
                    }
                }
            }
            if ($content == "") {
                $ret = $this->DeleteArticle($id);
            } else {
                $ret = $this->InsertArticleContent($id, $author, $content);
            }
            if ($ret < 0) {
                $this->ulWikiHTML->SetBasicSettings("Edit failed");
                $this->ulWikiHTML->SetContent("Edit failed");
            } else if ($ret == 1) {
            } else {
                $this->ulWikiHTML->SetBasicSettings("Edit succeed");
                $this->ulWikiHTML->SetContent("Edit succeed");
            }
        }
        $this->ulWikiHTML->SetHeader($this->ulWikiVersion, 2);
        $this->ulWikiHTML->SetFooter($name."/", 0);
        $this->ulWikiOutputBuffer = $this->ulWikiHTML->Generate();
        return $ret;
    }

    /**
     * Search form HTML render and write it to ulWikiHTML
     * @return int 0=success -1=failed
     */
    function SearchFormHTMLRender() {
        $this->SetArticleName("Search");
        $this->ulWikiHTML->SetBasicSettings($this->ulArticleName);
        $this->ulWikiHTML->SetHeader($this->ulWikiVersion, 3);
        $html = '<form action="./" method="GET"><input type="text" name="s"><input type="submit"></form><br>'."\r\n";
        $html .= "<hr>\r\n";
        $html .= "Latest updates:<br>\r\n";
        if ($list = $this->ulDB->selectWikiListLatest(0, 9)) {
            $html .= $this->GenerateArrayHTML($list);
        } else {
            $html .= "No results found<br>\r\n";
        }
        $html .= '<a href="?s&last">more...</a><br>'."\r\n";
        $html .= "<hr>\r\n";
        $html .= "Special pages:<br>\r\n";
        $html .= '<a href="?s=wp">WriteProtected</a> <a href="?s&meta">MetadataList</a>'."\r\n";
        $this->ulWikiHTML->SetContent($html);
        $this->ulWikiHTML->SetLastUpdate($this->ulArticleCreatedTime);
        $this->ulWikiHTML->SetFooter("./", 0);
        $this->ulWikiOutputBuffer = $this->ulWikiHTML->Generate();
        return 0;
    }

    /**
     * Search result HTML render and write it to ulWikiHTML
     * @param string $aname search name
     * @param int $mode 0=search article 1=latest
     * @return int 0=success -1=failed
     */
    function SearchResultHTMLRender($aname, $page, $mode) {
        try {
            if ($page < 1) {
                $pn = 1;
            } else {
                $pn = $page;
            }
            $tp = -1;
            if ($mode === 0) {
                if ($aname == "wp") {
                    if (!$list = $this->ulDB->selectWikiListWriteProtected(($pn - 1) * 9, 9))
                        throw new Exception('No results found');
                    $tp = $this->ulDB->selectWikiNumberWriteProtected();
                } else {
                    if (!$list = $this->ulDB->selectWikiNameInclude($aname, ($pn - 1) * 9, 9))
                        throw new Exception('No results found');
                    $tp = $this->ulDB->selectWikiNumberNameInclude($aname);
                }
                $this->SetArticleName("Search - ".$this->ulWikiHTML->SanitizeString($aname, 0));
                $html = "Search result:<br>\r\n".$this->GenerateArrayHTML($list);
                $html .= $this->GeneratePagerHTML($pn, ceil($tp / 9), $this->ulWikiPath."?s=".$aname."&p=");
            } else {
                if (!$list = $this->ulDB->selectWikiListLatest(($pn - 1) * 9, 9))
                    throw new Exception('No results found');
                $tp = $this->ulDB->selectWikiTotalArticleNumber();
                $this->SetArticleName("Latest updates");
                $html = "Latest updates:<br>\r\n".$this->GenerateArrayHTML($list);
                $html .= $this->GeneratePagerHTML($pn, ceil($tp / 9), $this->ulWikiPath."?s&last&p=");
            }
        } catch (Exception $e) {
            $this->SetArticleName($e->getMessage());
            $html = $e->getMessage()."<br>\r\n";
        }
        $this->ulWikiHTML->SetBasicSettings($this->ulArticleName);
        $this->ulWikiHTML->SetHeader($this->ulWikiVersion, 3);
        $this->ulWikiHTML->SetContent($html);
        $this->ulWikiSetHeader->SetLastModified(time());
        $this->ulWikiHTML->SetFooter("./?s", 0);
        $this->ulWikiOutputBuffer = $this->ulWikiHTML->Generate();
        return 0;
    }

    /**
     * Newpage Form Render
     * @return int
     */
    public function NewpageFormHTMLRender() {
        $this->SetArticleName("CreateNewPage");
        $html = '<form action="./" method="GET"><input type="text" name="n"><input type="submit"></form><br>'."\r\n";
        $this->ulWikiHTML->SetBasicSettings($this->ulArticleName);
        $this->ulWikiHTML->SetHeader($this->ulWikiVersion, 3);
        $this->ulWikiHTML->SetContent($html);
        $this->ulWikiSetHeader->SetLastModified(time());
        $this->ulWikiHTML->SetFooter("./", 0);
        $this->ulWikiOutputBuffer = $this->ulWikiHTML->Generate();
        return 0;
    }

    /**
     * Newpage Redirect Render
     * @param string $aname article name
     * @return int
     */
    public function NewpageRedirectHTMLRender($aname) {
        $this->SetArticleName("NewPageRedirect - ".$this->ulWikiHTML->SanitizeString($aname, 0));
        $html = '<a href="'.$this->ulWikiHTML->SanitizeString($aname, 0).'/">'.$this->ulWikiHTML->SanitizeString($aname, 0).'</a>'."\r\n";
        $this->ulWikiHTML->SetBasicSettings($this->ulArticleName);
        $this->ulWikiHTML->SetHeader($this->ulWikiVersion, 3);
        $this->ulWikiHTML->SetContent($html);
        $this->ulWikiSetHeader->SetLastModified(time());
        $this->ulWikiHTML->SetFooter("./", 0);
        $this->ulWikiOutputBuffer = $this->ulWikiHTML->Generate();
        return 0;
    }

    /**
     * Metadata Editor Render
     * @param string $name article name
     * @return int
     */
    public function MetadataHTMLRender($name) {
        if (!$this->IsValidArticleName($name)) {
            $this->LoadArticleInvalid();
        } elseif ($id = $this->ulDB->selectWikiArticleIDfromName($name)) {
            $this->LoadArticle($id);
            $this->LoadArticleMeta($id);
            $this->SetArticleName($name);
        } else {
            $this->LoadArticleEditNotfound($name);
        }
        $this->ulWikiHTML->SetBasicSettings($this->ulArticleName." - MetaEdit");
        $this->ulWikiHTML->SetHeader($this->ulWikiVersion, 2);
        $html = '<form action="?m&name='.$name.'" method="POST">short(2047): <input type="text" name="contentshort" value="'.$this->ulWikiHTML->SanitizeString($this->ulMetaContent, 0).'"><br>'."\r\n";
        $html .= 'category: <select name="category">';
        for ($i = 0; $i < count($this->ulCategory); $i++) {
            $html .= '<option value="'.$i.'"';
            if ($i == $this->ulMetaCategory) {
                $html .= " selected";
            }
            $html .= '>'.$this->ulCategory[$i].'</option>';  
        }
        for ($i = 0; $i < 3;) {
            $j = $i + 128;
            $html .= '<option value="'.$j.'"';
            if ($j == $this->ulMetaCategory) {
                $html .= " selected";
            }
            $html .= '>user'.++$i.'</option>';
        }
        $html .= '</select> rate: <select name="rate">';
        for ($i = 0; $i < 11; $i++) {
            $html .= '<option value="'.$i;
            if ($i == $this->ulMetaRate) {
                $html .= '" selected>';
            } else {
                $html .= '">';
            }
            if ($i == 0) {
                $html .= 'none';
            } else {
                $html .= $i;
            }
            $html .= '</option>';
        }
            $html .= '</select><br>link(2047): <input type="text" name="link" value='.$this->ulMetaLink.'><br>'."\r\n";
        $html .= 'author(1023): <input type="text" name="manufacturer" value="'.$this->ulMetaManufacturer.'"><br>'."\r\n";
        $html .= 'latitude: <input type="text" name="latitude" value="'.$this->ulMetaLatitude.'"><br>'."\r\n";
        $html .= 'longitude: <input type="text" name="longitude" value="'.$this->ulMetaLongitude.'"><br>'."\r\n";
        $html .= '<input type="submit"></form>';
        $this->ulWikiHTML->SetContent($html);
        $this->ulWikiHTML->SetFooter("./", 0);
        $this->ulWikiOutputBuffer = $this->ulWikiHTML->Generate();
        return 0;
    }

    /**
     * Metadata Edit Result Render
     * @param string $name article name
     * @param int $category category id
     * @param string $link link url
     * @param string $manufacturer manufacturer or author
     * @param int $rate rating
     * @param string $contentshort short description of content
     * @param float $latitude latitude
     * @param float $longitude longitude
     * @return int
     */
    public function MetadataResultHTMLRender($name, $category, $link, $manufacturer, $rate, $contentshort, $latitude, $longitude) {
        $ret = 0;
        if (!$this->IsValidArticleName($name)) {
            $this->ulWikiHTML->SetBasicSettings("InvalidRequest");
            $this->LoadArticleInvalid();
            $this->ulWikiHTML->SetContent($this->ulArticleContent);
        } else {
            if ($this->IsValidMetadata($category, $link, $manufacturer, $rate, $contentshort, $latitude, $longitude) < 0) {
                $ret = -1;
            }
            if (!$id = $this->ulDB->selectWikiArticleIDfromName($name)) {
                if (!$id = $this->CreateArticle($name)) {
                    $ret = -1;
                }
            }
            if ($ret < 0) {
                $this->ulWikiHTML->SetBasicSettings("Edit failed");
                $this->ulWikiHTML->SetContent("Metadata edit failed");
            } else {
                if ($this->UpdateArticleMeta($id, $category, $link, $manufacturer, $rate, $contentshort, $latitude, $longitude) < 0) {
                    $this->ulWikiHTML->SetBasicSettings("Edit failed");
                    $this->ulWikiHTML->SetContent("Metadata edit failed");
                } else {
                    $this->ulWikiHTML->SetBasicSettings("Edit succeed");
                    $this->ulWikiHTML->SetContent("Metadata edit succeed");
                }
            }
        }
        $this->ulWikiHTML->SetHeader($this->ulWikiVersion, 2);
        $this->ulWikiHTML->SetFooter($name."/", 0);
        $this->ulWikiOutputBuffer = $this->ulWikiHTML->Generate();
        return $ret;
    }

    /**
     * Metadata HTML render and write it to ulWikiHTML
     * @param int $category category, 0 if null
     * @param int $page page
     * @return int 0=success -1=failed
     */
    public function MetaListHTMLRender($category, $page) {
        if (!isset($category) or !is_numeric($category) or $category > 254) {
            $metacat = 255;
        } else {
            $metacat = $category;
        }
        if ($metacat == 255) {
            $this->SetArticleName("MetadataList");
        } else {
            $this->SetArticleName("MetadataList - ".$this->ConvertCategoryIDtoName($metacat));
        }
        $this->ulWikiHTML->SetBasicSettings($this->ulArticleName);
        $this->ulWikiHTML->SetHeader($this->ulWikiVersion, 3);
        $html = '<a href="?s&meta">all</a> <a href="?s&meta=1">item</a> <a href="?s&meta=2">food</a> <a href="?s&meta=3">book</a> <a href="?s&meta=4">music</a> <a href="?s&meta=5">software</a> <a href="?s&meta=6">place</a> <a href="?s&meta=128">user1</a> <a href="?s&meta=129">user2</a> <a href="?s&meta=130">user3</a><br>'."\r\n";
        $html .= "<hr>\r\n";
        $html .= $this->ConvertCategoryIDtoName($metacat);
        $html .= ":<br>\r\n";
        if ($list = $this->ulDB->selectWikiArticleMetaList(($page - 1) * 9, 9, $metacat)) {
            $list = $this->InsertMetaArrayArticleName($list);
            $html .= $this->GenerateMetaArrayHTML($list);
            $tp = $this->ulDB->selectWikiTotalMetaNumber($metacat);
            $html .= $this->GeneratePagerHTML($page, ceil($tp / 9), $this->ulWikiPath."?s&meta=".$metacat."&p=");
        } else {
            $html .= "No results found<br>\r\n";
        }
        $this->ulWikiHTML->SetContent($html);
        $this->ulWikiHTML->SetFooter("./?s", 0);
        $this->ulWikiOutputBuffer = $this->ulWikiHTML->Generate();
        return 0;
    }

    /**
     * Get HTML from buffer
     * @return string
     */
    public function GetHTML() {
        return $this->ulWikiOutputBuffer;
    }

    public function __destruct() {
        // destruct child objects
        $this->ulDB = NULL;
        $this->ulWikiHTML = NULL;
    }
}

function wikimain() {
    global $dbfilename;
    $html = "";

    $ulwiki = new UlWiki($dbfilename);
    // if edit
    if (isset($_GET["e"])) {
        if (isset($_POST["content"])) {
            $ulwiki->EditorResultHTMLRender($_GET["name"], "wiki", $_POST["content"]);
            $html = $ulwiki->GetHTML();
        } else {
            $ulwiki->EditorHTMLRender($_GET["name"]);
            $html = $ulwiki->GetHTML();
        }
    } elseif(isset($_GET["s"])) {
        if (isset($_GET["last"])) {
            if (!isset($_GET["p"])) {
                $p = 1;
            } else {
                $p = $_GET["p"];
            }
            $ulwiki->SearchResultHTMLRender("", $p, 1);
            $html = $ulwiki->GetHTML();
        } elseif(isset($_GET["meta"])) {
            if (!isset($_GET["p"])) {
                $p = 1;
            } else {
                $p = $_GET["p"];
            }
            $ulwiki->MetaListHTMLRender($_GET["meta"], $p);
            $html = $ulwiki->GETHTML();
        } elseif($_GET["s"] != "") {
            if (!isset($_GET["p"])) {
                $p = 1;
            } else {
                $p = $_GET["p"];
            }
            $ulwiki->SearchResultHTMLRender($_GET["s"], $p, 0);
            $html = $ulwiki->GetHTML();
        } else {
            $ulwiki->SearchFormHTMLRender();
            $html = $ulwiki->GetHTML();
        }
    } elseif(isset($_GET["n"])) {
        if ($_GET["n"] != "") {
            $ulwiki->NewpageRedirectHTMLRender($_GET["n"]);
            $html = $ulwiki->GetHTML();
        } else {
            $ulwiki->NewpageFormHTMLRender();
            $html = $ulwiki->GetHTML(); 
        }
    } elseif(isset($_GET["m"])) {
        if ($_GET["name"] != "") {
            if (isset($_POST["category"]) and is_numeric($_POST["category"]) and $_POST["category"] < 255 and $_POST["category"] >= 0) {
                $ulwiki->MetadataResultHTMLRender($_GET["name"], $_POST["category"], $_POST["link"], $_POST["manufacturer"], $_POST["rate"], $_POST["contentshort"], $_POST["latitude"], $_POST["longitude"]);
                $html = $ulwiki->GetHTML(); 
            } else {
                $ulwiki->MetadataHTMLRender($_GET["name"]);
                $html = $ulwiki->GetHTML(); 
            }
        }
    } else {
        // if article name is specified
        if (isset($_GET["name"]) && $_GET["name"] != "") {
            $ulwiki->ArticleHTMLRender($_GET["name"]);
            $html = $ulwiki->GetHTML();
        } else {
            $ulwiki->ArticleTopHTMLRender();
            $html = $ulwiki->GetHTML(); 
        }
    }
    echo $html;
}

require("ul-set.php");
require("ul-db.php");

wikimain();