<?php
// Uler user uler/user/

function usercheckaddtag($tag, $mpass, $pass, $info, $until) {
    global $upcommentlimit;

    if (isset($tag) === false or mb_strlen($tag) > 20 or $tag == "empty") {
        return -1;
    }
    if (isset($info) === false or mb_strlen($info) > $upcommentlimit
        or $info == "empty") {
        return -2;
    }
    if (isset($pass) === false or ($pass != "" and preg_match('/\A[a-zA-Z0-9]{4,20}\z/', $pass) === 0)) {
        return -3;
    }
    if (isset($until) === false or ($until != "" and (is_numeric($until) === false or $until > 3471292800 or $until < 0))) {
        return -4;
    }
    if (isset($mpass) === false or $mpass == "") {
        return -5;
    }
    return 1;
}

function useraddtag($tag, $mpass, $pass, $info, $until) {
    global $dbfilename;

    $ret = usercheckaddtag($tag, $mpass, $pass, $info, $until);
    if ($ret < 0) {
        return $ret;
    }

    $uldb = new Uldb($dbfilename);
    $uldb->connect();
    $res = $uldb->selectUsers(0);
    if (password_verify($mpass, $res["pass"]) === false) {
        return -5;
    }
    $uldb->insertTags(htmlspecialchars($tag), $pass, htmlspecialchars($info), $until);
    return 1;
}

function usermain() {
    global $out;
    global $backto;
    global $headertitle;

    if (isset($_GET["tag"])) {
        //uler/user/tag
        if (isset($_POST["tag"])) {
            $res = useraddtag($_POST["tag"], $_POST["masterpass"], $_POST["pass"], $_POST["info"], $_POST["until"]);
            if (isset($_POST["json"])) {
                echo json_encode($res);
                exit();
            }
            if ($res < 0) {
                $out .= $res."Request failed";
            } else {
                $out .= "request succeed";
            }
        } else {
            $out .= "<form action=\"tag\" method=\"POST\">Tag: <input type=\"text\" name=\"tag\">Master pass: <input type=\"text\" name=\"masterpass\">Set Pass: <input type=\"text\" name=\"pass\">Info: <textarea name=\"info\"></textarea>until(unix): <input type=\"text\" name=\"until\"><input type=\"submit\"></form>";
        }
    }
}